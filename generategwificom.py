import random
import sys

def genComm(src, nbSTA, size, nbCom, delay,start):
    return str(str(src)+",OUT,"+str(size)+","+str(nbCom)+","+str(delay)+","+str(start))


if(len(sys.argv) < 3):
    print("usage: "+sys.argv[0]+" <nbSTA> <durComm> [seed]")
    exit(1)

if(len(sys.argv) == 4):
    random.seed(sys.argv[3])

nbSTA=int(sys.argv[1]) # total number of stations
durTot=int(sys.argv[2])

dataTot = 0
for i in range(nbSTA):
    # modem like behaviour
    if(i%10==0):
        delay=10
        size = random.randint(1024*1024, 1024*1024*6)
        # always active
        starttime = 3
        nbCom = max(1,(durTot-4)//10)
    # smartphone like behavious
    else:
        delay = 10
        size = 1024*1024*2
        starttime = max(3,random.randint(1,durTot-10))
        # amount of messages sent
#        print((durTot-starttime)//10)
        nbCom = max(1,random.randint(1,(durTot-starttime)//10))

    print(genComm(i, nbSTA, size, nbCom, delay, starttime))
#print(str(dataTot)+" bytes to transfer")
