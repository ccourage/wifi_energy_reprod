import random
import sys

if(len(sys.argv) < 2):
    print("usage: "+sys.argv[0]+" <nbSTA> [seed]")
    exit(1)

if(len(sys.argv) == 3):
  random.seed(sys.argv[2])

nbSTA=int(sys.argv[1]) # total number of stations

dataTot = 0

# generates a list of src-dst between which flows are created
for i in range(nbSTA):
  dst = random.randint(0, nbSTA)
  while (dst == i):
    dst = random.randint(0, nbSTA)
  print("{},{}".format(i, dst))