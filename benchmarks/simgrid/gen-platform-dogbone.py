#!/usr/bin/python

import sys, os
wai=os.path.dirname(os.path.abspath(__file__))

if len(sys.argv) != 5:
    print("Usage: ./"+os.path.basename(__file__)+" <nSTAPerAP> <bandwidths> <factor> <cancelidleEnergy>")
    exit(1)
else:
    nSTAPerAP=int(sys.argv[1])
    bandwidth=sys.argv[2]
    factor=float(sys.argv[3])
    cancelIDLEEnergy=int(sys.argv[4])
    if(cancelIDLEEnergy):
      idleE=0
    else:
      idleE=0.82
def pwrite(text,override=False):
    print(text+"\n")



header="""<?xml version='1.0'?>

<!DOCTYPE platform SYSTEM "https://simgrid.org/simgrid.dtd">
<platform version="4.1">
  <zone id="world" routing="Full">

"""
footer="""
    <!-- Wired AS -->
    <zone id="Wired zone" routing="Full">
      <host id="NODE1" speed="100.0Mf,50.0Mf,20.0Mf" />
      <host id="NODE2" speed="100.0Mf,50.0Mf,20.0Mf" />
      <link id="dogBonel" sharing_policy="SHARED" bandwidth="10Gbps" latency="0ms" />
      <route src="NODE1" dst="NODE2"><link_ctn id="dogBonel"/></route>
    </zone>


    <!-- Links -->
    <link id="Collector1" sharing_policy="SHARED" bandwidth="10Gbps" latency="0ms" />
    <link id="Collector2" sharing_policy="SHARED" bandwidth="10Gbps" latency="0ms" />
    <link id="Collector3" sharing_policy="SHARED" bandwidth="10Gbps" latency="0ms" />
    <link id="Collector4" sharing_policy="SHARED" bandwidth="10Gbps" latency="0ms" />

    <!-- Route to the wired zone -->
    <zoneRoute src="wlan1" dst="Wired zone" gw_src="WIFIrouter1" gw_dst="NODE1">
      <link_ctn id="Collector1" />
    </zoneRoute>
    <zoneRoute src="wlan2" dst="Wired zone" gw_src="WIFIrouter2" gw_dst="NODE2">
      <link_ctn id="Collector2" />
    </zoneRoute>
    <zoneRoute src="wlan3" dst="Wired zone" gw_src="WIFIrouter3" gw_dst="NODE1">
      <link_ctn id="Collector3" />
    </zoneRoute>
    <zoneRoute src="wlan4" dst="Wired zone" gw_src="WIFIrouter4" gw_dst="NODE2">
      <link_ctn id="Collector4" />
    </zoneRoute>


    <!-- Route between wlans -->
    <!-- wlan1 <-> wlan2 -->
    <zoneRoute src="wlan1" dst="wlan2" gw_src="WIFIrouter1" gw_dst="WIFIrouter2">
      <link_ctn id="Collector1"/>
      <link_ctn id="dogBonel"/>
      <link_ctn id="Collector2"/>
    </zoneRoute>

    <!-- wlan1 <-> wlan3 -->
    <zoneRoute src="wlan1" dst="wlan3" gw_src="WIFIrouter1" gw_dst="WIFIrouter3">
      <link_ctn id="Collector1"/>
      <link_ctn id="Collector3"/>
    </zoneRoute>

    <!-- wlan1 <-> wlan4 -->
    <zoneRoute src="wlan1" dst="wlan4" gw_src="WIFIrouter1" gw_dst="WIFIrouter4">
      <link_ctn id="Collector1"/>
      <link_ctn id="dogBonel"/>
      <link_ctn id="Collector4"/>
    </zoneRoute>

    <!-- wlan2 <-> wlan3 -->
    <zoneRoute src="wlan2" dst="wlan3" gw_src="WIFIrouter2" gw_dst="WIFIrouter3">
      <link_ctn id="Collector2"/>
      <link_ctn id="dogBonel"/>
      <link_ctn id="Collector3"/>
    </zoneRoute>

    <!-- wlan2 <-> wlan4 -->
    <zoneRoute src="wlan2" dst="wlan4" gw_src="WIFIrouter2" gw_dst="WIFIrouter4">
      <link_ctn id="Collector2"/>
      <link_ctn id="Collector4"/>
    </zoneRoute>

    <!-- wlan3 <-> wlan4 -->
    <zoneRoute src="wlan3" dst="wlan4" gw_src="WIFIrouter3" gw_dst="WIFIrouter4">
      <link_ctn id="Collector3"/>
      <link_ctn id="dogBonel"/>
      <link_ctn id="Collector4"/>
    </zoneRoute>

  </zone>
</platform>
"""

def buildT():

    for i in range(1,5):
        pwrite("""    <zone id="wlan{}" routing="Wifi">
        <!-- First declare the Access Point (ie, the wifi media) -->
        <prop id="access_point" value="WIFIrouter{}" />
""".format(i,i))
        pwrite("""    <!-- Declare the stations of this wifi zone -->""")
        for j in range(nSTAPerAP):
            pwrite("""    <host id="STA{}" speed="100.0Mf,50.0Mf,20.0Mf"></host>""".format((i-1)*nSTAPerAP+j))
        pwrite("""         <link id="AP{}" sharing_policy="WIFI" bandwidth="{}"  latency="0ms">
                  <prop id = "control_duration" value = "{}"/>
                  <prop id = "wifi_watt_values" value = "{}:1.14:0.94:0.10"/>
        </link>
        <router id="WIFIrouter{}"/>
    </zone>""".format(i,bandwidth,factor,idleE,i))


pwrite(header,True)
buildT()
pwrite(footer)