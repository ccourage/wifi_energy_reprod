#!/bin/bash

wai=$(dirname $(readlink -f $0))
plots=${wai}/plots
org=${plots}/pdf.org

say (){
    echo -en "$@" >> "$org"
}

echo -n > "$org"
say "#+TITLE: Data Analysis\n"
say "#+AUTHOR: Loic GUEGAN\n"

say "\\\newpage\n"
say "* Scenarios\n"
say "** Topology 1\n"
say "Topology 1 (topo=1) uses 1 Access Point with pairs of stations communicating together via the Access Point. 
The communication can be ether unidirectional (flow=1) or bidirectional (flow=2). 
The number of station pairs can be varied (nNodePair parameter). The amount of data send by each node can be varied (using dataSize parameter).\n"
say "#+attr_latex: :width 200px\n"
say "[[file:${wai}/../paper/figures/platforms/T1.png]]\n"
say "** Topology 2\n"
say "Topology 2 (topo=2) uses 1 Access Point with several stations communicating with the Access Point.
The communications are unidirectional only (STA => AP).
The number of station can be varied (using the nNode parameter).
The amount of data send by each node can be varied (using dataSize parameter).\n"
say "#+attr_latex: :width 180px\n"
say "[[file:${wai}/../paper/figures/platforms/T2.png]]\n"



for date in $(find "$plots"/* -type d )
do
    date=$(basename "$date")
    say "* $(date -d @$date)\n"
    for file in $(find "$plots/$date" -type f -name "*.png")
    do
        say "#+attr_latex: :width 400px\n"
        say "[[file:$file]]\n"
    done
done

emacs $org --batch -f org-latex-export-to-pdf --kill
