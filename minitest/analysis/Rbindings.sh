# File to source!

# Please define this variable before sourcing
[ -z ${bindLibPath+x} ] && bindLibPath="./"

# Parameters
tools=${bindLibPath}/lib/tools.R

Rrun (){
    file=$1 && shift
    R --slave -e 'source("'$file'");'$@ 2>&1
}

randRange (){
    n=$1
    min=$2
    max=$3
    Rrun $tools "randRange($n,$min,$max)"
}
