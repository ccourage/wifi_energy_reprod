User Guide
===

## Setup SimGrid and ns-3 *(run directory)*
- **SimGrid**:
	- Clone SimGrid repo into run/simgrid/simgrid
	- Compile SimGrid into run/simgrid/simgrid/build
- **ns-3**:
	- Extract ns-3 source code into run/ns-3/ns-3
	- Compile ns-3

## Grid5000 (G5K) Experiments *(run directory)*
**Scripts:**
- `run/subscribe.py` Subscribe nodes on G5K with enoslib
- `run/setup_g5k.sh`  Deploy ns3 and SimGrid on G5K
- `run/run_g5k.sh` Deploy simulators on G5K and launch the simulations
- `run/ask.sh` Have a look on the ongoing G5K simulations

**How to proceed:**
- First goto the run directory and then execute *./subscribe.py*
- Then *./setup_g5k.sh* (you can do it only one time, then simulators will still be present on G5K dfs)
- Next run *./run_g5k.sh* to launch the simulations (and follow the progression with *ask.sh*)
- Simulation arguments will be placed into *run/args.sh*
- Simulation results into *run/results.csv*

## Data Analysis *(analysis directory)*
**Scripts:**
- `analysis/run-analysis.R`Main data analysis script (call the genAll() function to generate every plots into the run/plots directory)
- `analysis/lib/decayModel.R` Contains various R function for the decay model (implicitely used by *run/analysis.R*)
- `analysis/gen-pdf.sh` Combines every plot from the "analysis/plots/" directory into a pdf file into analysis/plots/pdf.pdf

## Notes
- You can run experiments on your local machine with the script *run/run.sh*
- `archives` Directory contains archived files
- Paper directory contains the written article based on the G5K experimentations
