/* Copyright (c) 2017-2018. The SimGrid Team. All rights reserved.          */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#include <simgrid/s4u.hpp>
#include <simgrid/s4u/Host.hpp>
#include "xbt/log.h"
#include "src/kernel/resource/WifiLinkImpl.hpp"
#include <exception>
#include <iostream>
#include <random>
#include <sstream>
#include <string>

XBT_LOG_NEW_DEFAULT_CATEGORY(simulator, "[wifi_usage] 1STA-1LINK-1NODE-CT");

double DURATION=180;

/**
 * @brief Setup the simulation flows
 * @param topo Topology id
 * @param dataSize Amount of data to send in bytes
 * @param flowConf The flow configuration
 */
void setup_simulation(short topo, int dataSize, short flowConf);
/**
 * @brief Function used as a sending actor
 */
static void send(std::vector<std::string> args);
/**
 * @brief Function used as a receiving actor
 */
static void rcv(std::vector<std::string> args);
/**
 * @brief Set rate of a station on a wifi link
 * @param ap_name Name of the access point (WIFI link)
 * @param node_name Name of the station you want to set the rate
 * @param rate The rate id
 */
void set_rate(std::string ap_name, std::string node_name, short rate);

/**
 * Usage ./simulator <platformFile> <topo> <dataSize> <flowConf>
 * All arguments are mandatory.
 */
int main(int argc, char **argv) {

  // Build engine
  simgrid::s4u::Engine engine(&argc, argv);
  engine.load_platform(argv[1]);

  // Parse arguments
  short topo=std::stoi(argv[2]);
  int dataSize=std::stoi(argv[3]);
  int flowConf=std::stoi(argv[4]);
  XBT_INFO("topo=%d dataSize=%d flowConf=%d",topo,dataSize,flowConf);

  // Setup/Run simulation
  setup_simulation(topo,dataSize,flowConf);
  engine.run();
  XBT_INFO("Simulation took %fs", simgrid::s4u::Engine::get_clock());

  return (0);
}

void setup_simulation(short topo, int dataSize, short flowConf) {
  std::vector<std::string> noArgs;

  if(topo==1){
    int nHost=simgrid::s4u::Engine::get_instance()->get_host_count();
    for(int i=0;i<nHost-1;i+=2){
      std::ostringstream ss;
      ss<<"STA"<<i;
      std::string STA_A=ss.str();
      ss.str("");
      ss<<"STA"<<i+1;
      std::string STA_B=ss.str();

      if(flowConf==1){
        std::vector<std::string> args_STA_A;
        args_STA_A.push_back(STA_B);
        args_STA_A.push_back(std::to_string(dataSize));

        simgrid::s4u::Actor::create(STA_A, simgrid::s4u::Host::by_name(STA_A),
                                    send, args_STA_A);
        simgrid::s4u::Actor::create(STA_B, simgrid::s4u::Host::by_name(STA_B),
                                    rcv, noArgs);
      }
      else if(flowConf==2){
        // --------------------------- 1
        // Send to B
        std::vector<std::string> args_STA_A;
        args_STA_A.push_back(STA_B);
        args_STA_A.push_back(std::to_string(dataSize));
        simgrid::s4u::Actor::create(STA_A+"Send", simgrid::s4u::Host::by_name(STA_A),
                                    send, args_STA_A);
        // Receive from A
        simgrid::s4u::Actor::create(STA_B, simgrid::s4u::Host::by_name(STA_B),
                                    rcv, noArgs);
        // --------------------------- 2
        // Send to A
        std::vector<std::string> args_STA_B;
        args_STA_B.push_back(STA_A);
        args_STA_B.push_back(std::to_string(dataSize));
        simgrid::s4u::Actor::create(STA_B+"Send", simgrid::s4u::Host::by_name(STA_B),
                                    send, args_STA_B);
        // Receive from B
        simgrid::s4u::Actor::create(STA_A, simgrid::s4u::Host::by_name(STA_A),
                                    rcv, noArgs);
      }
      set_rate("AP1",STA_A,0);
      set_rate("AP1",STA_B,0);
    }
  }
  else if(topo==2){
    int nHost=simgrid::s4u::Engine::get_instance()->get_host_count();

    for(int i=0;i<nHost-1;i+=1){ // -1 because of the AP
          XBT_INFO("%d",nHost);
      std::ostringstream ss;
      ss<<"STA"<<i;
      std::string STA=ss.str();
      std::vector<std::string> args_STA;
      args_STA.push_back("AP");
      args_STA.push_back(std::to_string(dataSize));

      simgrid::s4u::Actor::create(STA, simgrid::s4u::Host::by_name(STA),
                                  send, args_STA);

      set_rate("cell",STA,0);
      // One receiver for each flow
      simgrid::s4u::Actor::create("AP", simgrid::s4u::Host::by_name("AP"),
                                  rcv, noArgs);
    }
    set_rate("cell", "AP", 0);
  }
}

void set_rate(std::string ap_name, std::string node_name, short rate) {
  simgrid::kernel::resource::WifiLinkImpl *l =
      (simgrid::kernel::resource::WifiLinkImpl *)simgrid::s4u::Link::by_name(
          ap_name)
          ->get_impl();
  l->set_host_rate(simgrid::s4u::Host::by_name(node_name), rate);
  //bool enabled=l->toggle_decay_model();
  // if(!enabled)
  //   l->toggle_decay_model();
}

static void rcv(std::vector<std::string> args) {
  std::string selfName = simgrid::s4u::this_actor::get_host()->get_name();
  simgrid::s4u::Mailbox *selfMailbox = simgrid::s4u::Mailbox::by_name(
      simgrid::s4u::this_actor::get_host()->get_name());

  selfMailbox->get<std::string>();
  simgrid::s4u::this_actor::sleep_until(DURATION);
}

static void send(std::vector<std::string> args) {
  std::string selfName = simgrid::s4u::this_actor::get_host()->get_name();
  simgrid::s4u::Mailbox *selfMailbox = simgrid::s4u::Mailbox::by_name(
      simgrid::s4u::this_actor::get_host()->get_name());

  simgrid::s4u::Mailbox *dstMailbox =
    simgrid::s4u::Mailbox::by_name(args.at(0));

  int dataSize = std::atoi(args.at(1).c_str());
  double comStartTime = simgrid::s4u::Engine::get_clock();
  dstMailbox->put(const_cast<char *>("message"), dataSize);
  double comEndTime = simgrid::s4u::Engine::get_clock();
  XBT_INFO("%s sent %d bytes to %s in %f seconds from %f to %f",
           selfName.c_str(), dataSize, args.at(0).c_str(),
           comEndTime - comStartTime, comStartTime, comEndTime);
  simgrid::s4u::this_actor::sleep_until(DURATION);
}
