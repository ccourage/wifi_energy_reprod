#!/usr/bin/awk -f

BEGIN {
    consTots=0
    consDyns=0
    consStat=0
}

/sent/ {
    gsub("]","")
    simTime=$2+0
}

#/Simulation took/ {
#    simTime=$5+0
#}


/consumed/ {
    consDyns+=$10
    consStat+=$12
    consTots+=$7
    durIdle+=$14
    durTxRx+=$16
}

END {
    print("simTime="simTime";consStat="consStat";consDyns="consDyns";consTots="consTots";durIdle="durIdle";durTxRx="durTxRx";durRx=NA;durTx=NA;durBusy=NA")
}
