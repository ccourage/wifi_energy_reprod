#!/usr/bin/python

import sys, os
wai=os.path.dirname(os.path.abspath(__file__))

if len(sys.argv) != 7:
    print("Usage: ./"+os.path.basename(__file__)+" <topo> <nNodePair> <nNode> <bitRate> <correction-factor> beaconsf")
    exit(1)
else:
    topo=int(sys.argv[1])
    nNodePair=int(sys.argv[2])
    nNode=int(sys.argv[3])
    WIFI_BW="{}Mbps".format(float(sys.argv[4])*float(sys.argv[5]))
    beacons_f = float(sys.argv[6])

def pwrite(text,override=False):
    print(text+"\n")



header="""<?xml version='1.0'?><!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd"><platform version="4.1"><zone id="world" routing="Full">"""
footer="""  </zone>
</platform>"""

def buildT1():
    pwrite("""<zone id="WIFI zone" routing="Wifi">""")
#    pwrite("""<link id="AP1" sharing_policy="WIFI" bandwidth="{}" latency="0ms"></link>""".format(WIFI_BW)) #<prop id="beacons" value="false"/>
    pwrite("""<prop id="access_point" value="WIFI router" />""")
    for i in range(0,2*nNodePair):
        pwrite("""<host id="STA{}" speed="100.0Mf,50.0Mf,20.0Mf" />""".format(i))
    pwrite("""        <link id="AP1" sharing_policy="WIFI" bandwidth="{}" latency="0ms"><prop id="control_duration" value="{}"/></link>""".format(WIFI_BW, beacons_f))
    pwrite("""<router id="WIFI router"/>""")
    pwrite("</zone>")

def buildT2():
    pwrite("""<host id="AP" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0"></host>""")
    for i in range(0,nNode):
        pwrite("""<host id="STA{}" speed="100.0Mf,50.0Mf,20.0Mf" pstate="0"></host>""".format(i))
    pwrite("""<link id="cell" bandwidth="{}" latency="0ms" sharing_policy="WIFI"></link>""".format(WIFI_BW))
    for i in range(0,nNode):
        pwrite("""<route src="STA{}" dst="AP" symmetrical="YES"><link_ctn id="cell" /></route>""".format(i))



if topo==1:
    pwrite(header,True)
    buildT1()
    pwrite(footer)
elif topo==2:
    pwrite(header,True)
    buildT2()
    pwrite(footer)
else:
    print("Topology unknown.")

