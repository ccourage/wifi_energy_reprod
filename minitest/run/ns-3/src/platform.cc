#include "modules.hpp"
#include "ns3/pointer.h"
#include "ns3/uinteger.h"
#include "ns3/applications-module.h"

ApplicationContainer Sinks;

/**
 * @brief Make build application
 * @param dst Destination IP address
 * @param port Communication port
 * @param dataSize Amount of data to send
 * @param nodes Node to install the application
 */
void txApp(Ipv4Address dst, int port,int dataSize,NodeContainer nodes){
  BulkSendHelper echoClientHelper ("ns3::TcpSocketFactory",InetSocketAddress (dst, port));
  echoClientHelper.SetAttribute ("MaxBytes", UintegerValue (dataSize));
  echoClientHelper.Install(nodes).Start (Seconds (10.0)); // Here we start at 10s !!
  NS_LOG_UNCOND("TXAPP created for "<< dst);
}

/**
 * @brief Setup position/mobility on a wifi cell
 *
 * Node are spread homogeneously in circle around the access point.
 *
 * @param STA Stations in the wifi cell
 * @param AP Access Point of the wifi cell
 * @param The Stations circle radius
 */
void SetupMobility(NodeContainer STA, NodeContainer AP, double radius){
  // Define sensors position/mobility
  MobilityHelper mobilitySTA;
  mobilitySTA.SetMobilityModel ("ns3::ConstantPositionMobilityModel"); // Sensors are fixed
  mobilitySTA.SetPositionAllocator("ns3::UniformDiscPositionAllocator");  // With default value AP should be on the center... maybe... I hope.. I really hope because it works
  mobilitySTA.Install(NodeContainer(STA));

  // Ap on the center
  MobilityHelper mobilityAP;
  mobilityAP.SetMobilityModel ("ns3::ConstantPositionMobilityModel"); // Sensors are fixed
  mobilityAP.SetPositionAllocator("ns3::UniformDiscPositionAllocator",
                                "rho",DoubleValue(radius));
  mobilityAP.Install(AP);
}

/**
 * @brief Setup a 802.11n stack on STA and AP.
 * @param STA The cell's stations
 * @param AP the cell's Acces Point
 * @return The wifi cell net devices
 */
WifiNetDev SetupWifi(NodeContainer STA, NodeContainer AP){
  YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();
  YansWifiPhyHelper wifiPhy;// = YansWifiPhyHelper::Default ();
  wifiPhy.SetChannel (channel.Create ());

  WifiHelper wifiHelper;
  wifiHelper.SetStandard (WIFI_STANDARD_80211n_5GHZ);
  wifiHelper.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
				      "DataMode", StringValue ("HtMcs3"),//("HtMcs7"),
				      "ControlMode", StringValue ("HtMcs3")); //("HtMcs0"));

  /* Configure AP */
  WifiMacHelper wifiMac;
  Ssid ssid = Ssid ("net1");
  wifiMac.SetType ("ns3::ApWifiMac", "Ssid", SsidValue (ssid));
  NetDeviceContainer apNetDevice;
  apNetDevice = wifiHelper.Install (wifiPhy, wifiMac, AP);

  /* Configure STA */
  wifiMac.SetType ("ns3::StaWifiMac", "Ssid", SsidValue (ssid));
  NetDeviceContainer sensorsNetDevices;
  sensorsNetDevices = wifiHelper.Install (wifiPhy, wifiMac, STA);
  return(std::make_pair(apNetDevice, sensorsNetDevices));
}

/**
 * @brief Configure Applications (flow from SimGrid perspective) for topology 1
 *
 * @param AP The wifi Access Point
 * @param STA The wifi stations
 * @param APNET The @a AP net device
 * @param STANET The @a STA net devices
 * @param flowConf if 1 unidirectional and if 2 bidirectional
 * @param dataSize Number of data to send per application
 * @param nNodepair Number of pair of node (can be deduce from STANET but I am lazy)
 */
void SetFlowConfT1(NodeContainer AP,NodeContainer STA, NetDeviceContainer APNET,NetDeviceContainer STANET,short flowConf, int dataSize, short nNodePair){
  // Install TCP/IP stack & assign IP addresses
  InternetStackHelper internet;
  internet.Install (NodeContainer(AP,STA));


  Ipv4InterfaceContainer interfaces;
  Ipv4AddressHelper ipv4;
  ipv4.SetBase ("10.0.0.0", "255.255.0.0");
  interfaces=ipv4.Assign(NetDeviceContainer(APNET,STANET));

  int port=80;
  PacketSinkHelper STASink("ns3::TcpSocketFactory",InetSocketAddress (Ipv4Address::GetAny (), port));
  std::cout << "setup flow"<<std::endl;
  for(int i=0;i<(nNodePair*2-1);i+=2){
    std::cout << "flow "<<i<<std::endl;
    Ipv4Address IPnode1,IPnode2; // Will be fill in the following for loop
    IPnode1=interfaces.GetAddress(i+1); // Note first interface == AP!!!
    IPnode2=interfaces.GetAddress(i+2);

    if(flowConf==1){
      txApp(IPnode2,port,dataSize,STA.Get(i));
      Sinks.Add(STASink.Install(STA.Get(i+1))); // Install sink on last node
    }
    else if(flowConf==2){
      txApp(IPnode2,port,dataSize,STA.Get(i));
      Sinks.Add(STASink.Install(STA.Get(i+1))); // Install sink on last node

      txApp(IPnode1,port,dataSize,STA.Get(i+1));
      Sinks.Add(STASink.Install(STA.Get(i))); // Install sink on the first node
    }
  }
}


/**
 * @brief Configure Applications (flow from SimGrid perspective) for topology 1
 *
 * @param AP The wifi Access Point
 * @param STA The wifi stations
 * @param APNET The @a AP net device
 * @param STANET The @a STA net devices
 * @param flowConf if 1 unidirectional and if 2 bidirectional
 * @param dataSize Number of data to send per application
 */
void SetFlowConfT2(NodeContainer AP,NodeContainer STA, NetDeviceContainer APNET,NetDeviceContainer STANET,short flowConf, int dataSize){
  // Install TCP/IP stack & assign IP addresses
  InternetStackHelper internet;
  internet.Install (NodeContainer(AP,STA));

  Ipv4InterfaceContainer interfaces;
  Ipv4AddressHelper ipv4;
  ipv4.SetBase ("10.0.0.0", "255.255.0.0");
  interfaces=ipv4.Assign(NetDeviceContainer(APNET,STANET));

  NS_LOG_UNCOND("n=" << NetDeviceContainer(APNET,STANET).GetN());
  Ipv4Address IPAP; // Will be fill in the following for loop
  IPAP=interfaces.GetAddress(0);
  int port=80;

  txApp(IPAP,port,dataSize,STA);

  PacketSinkHelper apSink("ns3::TcpSocketFactory",InetSocketAddress (Ipv4Address::GetAny (), port));
  Sinks.Add(apSink.Install(AP.Get(0))); // Instal sink on last node
}


/**
* @brief Build Topology 1
 * @param nNodePair Number of pair of nodes which will communicate together
 * @param flowConf if 1 unidirectional and if 2 bidirectional
 * @param dataSize Number of data to send per application
 * @param radius The radius of the wifi cell
 */
std::vector<std::vector<DeviceEnergyModelContainer>> buildT1(short nNodePair, short flowConf, int dataSize, double radius){
  // Create nodes
  NodeContainer STA;
  STA.Create(2*nNodePair);
  NodeContainer AP;
  AP.Create(1);

  // Setup wifi
  WifiNetDev netDev=SetupWifi(STA,AP);
  SetupMobility(STA, AP, radius);

  // setup energy
  std::vector<std::vector<DeviceEnergyModelContainer>> energyModels;
  BasicEnergySourceHelper energySource;
  energySource.Set("BasicEnergySourceInitialEnergyJ", DoubleValue (10000000));
  EnergySourceContainer batteryAP = energySource.Install(AP);
  EnergySourceContainer batterySTA = energySource.Install(STA);

  WifiRadioEnergyModelHelper radioEnergyHelper;
  std::vector<DeviceEnergyModelContainer> emStat;
  emStat.push_back(radioEnergyHelper.Install(netDev.first, batteryAP));
  emStat.push_back(radioEnergyHelper.Install(netDev.second, batterySTA));

  radioEnergyHelper.Set("IdleCurrentA", DoubleValue (0));
  std::vector<DeviceEnergyModelContainer> emDyn;
  emDyn.push_back(radioEnergyHelper.Install(netDev.first, batteryAP));
  emDyn.push_back(radioEnergyHelper.Install(netDev.second, batterySTA));

  energyModels.push_back(emStat);
  energyModels.push_back(emDyn);
  /* Set Flow Configuration  */
  SetFlowConfT1(AP,STA,netDev.first,netDev.second,flowConf,dataSize,nNodePair);

  return energyModels;
}


/**
 * @brief Build Topology 2
 * @param nNodePair Number of pair of nodes which will communicate together
 * @param flowConf if 1 unidirectional and if 2 bidirectional
 * @param dataSize Number of data to send per application
 * @param radius The radius of the wifi cell
 */
std::vector<std::vector<DeviceEnergyModelContainer>> buildT2(short nNode, short flowConf, int dataSize, double radius){
  // Create nodes
  NodeContainer STA;
  STA.Create(nNode);
  NodeContainer AP;
  AP.Create(1);

  // Setup wifi
  WifiNetDev netDev=SetupWifi(STA,AP);
  SetupMobility(STA, AP,radius);

  // setup energy
  std::vector<std::vector<DeviceEnergyModelContainer>> energyModels;
  BasicEnergySourceHelper energySource;
  energySource.Set("BasicEnergySourceInitialEnergyJ", DoubleValue (10000000));
  EnergySourceContainer batteryAP = energySource.Install(AP);
  EnergySourceContainer batterySTA = energySource.Install(STA);

  WifiRadioEnergyModelHelper radioEnergyHelper;
  std::vector<DeviceEnergyModelContainer> emStat;
  emStat.push_back(radioEnergyHelper.Install(netDev.first, batteryAP));
  emStat.push_back(radioEnergyHelper.Install(netDev.second, batterySTA));

  radioEnergyHelper.Set("IdleCurrentA", DoubleValue (0));
  std::vector<DeviceEnergyModelContainer> emDyn;
  emDyn.push_back(radioEnergyHelper.Install(netDev.first, batteryAP));
  emDyn.push_back(radioEnergyHelper.Install(netDev.second, batterySTA));

  energyModels.push_back(emStat);
  energyModels.push_back(emDyn);

  /* Set Flow Configuration  */
  SetFlowConfT2(AP,STA,netDev.first,netDev.second,flowConf,dataSize);

  return energyModels;
}
