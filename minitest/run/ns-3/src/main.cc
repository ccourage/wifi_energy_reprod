#include "modules.hpp"
#ifndef NS3_VERSION
#define NS3_VERSION "unknown"
#endif
#include <memory>

// Good tuto: https://www.nsnam.org/docs/release/3.7/tutorial/tutorial_27.html

NS_LOG_COMPONENT_DEFINE ("WIFISensorsSimulator");

std::map<int, std::pair<int,std::vector<float>>> nodeTimers;
/*std::vector<int> lastUpdateStatus;
std::vector<int> nodeState;
std::vector<double> timeIDLE;
std::vector<double> timeRX;
std::vector<double> timeTX;
std::vector<double> timeCCA;*/

// https://stackoverflow.com/questions/2727749/c-split-string
std::vector<std::string> split(std::string text, std::string split_word) {
    std::vector<std::string> list;
    std::string word = "";
    int is_word_over = 0;

    for (int i = 0; i <= text.length(); i++) {
        if (i <= text.length() - split_word.length()) {
            if (text.substr(i, split_word.length()) == split_word) {
                list.insert(list.end(), word);
                word = "";
                is_word_over = 1;
            }
            //now we want that it jumps the rest of the split character
            else if (is_word_over >= 1) {
                if (is_word_over != split_word.length()) {
                    is_word_over += 1;
                    continue;
                }
                else {
                    word += text[i];
                    is_word_over = 0;
                }
            }
            else {
                word += text[i];
            }
        }
        else {
            word += text[i];
        }
    }
    list.insert(list.end(), word);
    return list;
}


void PhyStateTrace(std::string context, Time start, Time duration, enum WifiPhyState state)
{
  std::vector< std::string> sep = split(context, "/");
  float timeSecond = start.GetDouble()/1000000000;
  int idNode =  atoi(sep[2].c_str());

  /*if ( Simulator::Now ().GetSeconds () <= simulationTime && dti_Flag == 1)
  {*/
  if(nodeTimers.find(idNode) == nodeTimers.end()) {
    nodeTimers[idNode] = std::make_pair<int,std::vector<float>>(state, {timeSecond, 0,0,0,0,0,0,0});
  }
  else {
      switch (nodeTimers[idNode].first)
      {
      case IDLE:
        // m_idleStateFile << idNode << "," << lastUpdateStatus[idNode] << "," << timeSecond << std::endl;
        nodeTimers[idNode].second[1] += timeSecond - nodeTimers[idNode].second[0];
        nodeTimers[idNode].second[0] = timeSecond;
        nodeTimers[idNode].first = state;
        break;
      case RX:
        //m_rxStateFile << idNode << "," << lastUpdateStatus[idNode] << "," << timeSecond << std::endl;
        nodeTimers[idNode].second[2] += timeSecond - nodeTimers[idNode].second[0];
        nodeTimers[idNode].second[0] = timeSecond;
        nodeTimers[idNode].first = state;
        break;
      case TX:
        // m_txStateFile << idNode << "," << lastUpdateStatus[idNode] << "," << timeSecond << std::endl;
        nodeTimers[idNode].second[3] += timeSecond - nodeTimers[idNode].second[0];
        nodeTimers[idNode].second[0] = timeSecond;
        nodeTimers[idNode].first = state;
        break;
      case CCA_BUSY:
        // m_ccaStateFile << idNode << "," << lastUpdateStatus[idNode] << "," << timeSecond << std::endl;
        nodeTimers[idNode].second[4] += timeSecond - nodeTimers[idNode].second[0];
        nodeTimers[idNode].second[0] = timeSecond;
        nodeTimers[idNode].first = state;
        break;
      case SLEEP:
        //std::cout << context << " SLEEP " << std::endl;
        nodeTimers[idNode].second[5] += timeSecond - nodeTimers[idNode].second[0];
        nodeTimers[idNode].second[0] = timeSecond;
        nodeTimers[idNode].first = state;
        break;
      case SWITCHING:
        //std::cout << context << " SWITCHING " << std::endl;
        nodeTimers[idNode].second[6] += timeSecond - nodeTimers[idNode].second[0];
        nodeTimers[idNode].second[0] = timeSecond;
        nodeTimers[idNode].first = state;
        break;
      case OFF:
        //std::cout << context << " OFF " << std::endl;
        nodeTimers[idNode].second[7] += timeSecond - nodeTimers[idNode].second[0];
        nodeTimers[idNode].second[0] = timeSecond;
        nodeTimers[idNode].first = state;
        break;
      default:
        //std::cout << context << "VALEUR ETAT " << nodeState[idNode] << " DEFAULT " << std::endl;
        break;
    }
  }
  //}
}

/**
 * To get more details about functions please have a look at modules/modules.hpp
 */
int main(int argc, char* argv[]){


  uint32_t topo=1; // Choose you topology
  uint32_t flow=1; // 1=one way 1=bidirectional
  uint32_t dataSize=1000; // Choose the amount of data sended by nodes
  uint32_t nNodePair=1; // Number of sender/receiver pair
  uint32_t nNode=1; // Number of sender/receiver pair
  double radius=10; // Choose the distance between AP and STA
  int seed=10; // Choose the used by ns-3

  CommandLine cmd;
  cmd.AddValue ("topo", "Which topology to use", topo);
  cmd.AddValue ("flow", "Which flow configuration to use", flow);
  cmd.AddValue ("dataSize", "Packet data size sended by senders", dataSize);
  cmd.AddValue ("nNodePair", "Number of sender/receiver pairs", nNodePair);
  cmd.AddValue ("nNode", "Number of node", nNode);
  cmd.AddValue ("radius", "Radius between STA and AP", radius);
  cmd.AddValue ("seed", "Simulator seed", seed);
  cmd.Parse (argc, argv);

  ns3::RngSeedManager::SetSeed (seed);
  Config::SetDefault ("ns3::TcpSocket::SegmentSize", UintegerValue (1500));
  NS_LOG_UNCOND("WARNING: Transmissions start at T>10s. Thus, this delay should be removed from each line of logs. Note that if you use parse.awk, this delay is automatically removed."); LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
  //LogComponentEnable("PacketSink", LOG_LEVEL_INFO);
  //LogComponentEnable("BulkSendApplication", LOG_LEVEL_DEBUG);


  // ---------- Setup Simulations ----------
  std::vector<std::vector<DeviceEnergyModelContainer>> energyModels;

  if(topo==1)
    energyModels = buildT1(nNodePair,flow,dataSize,radius);
  else if(topo==2)
    energyModels = buildT2(nNode,flow,dataSize,radius);

  // Setup channel size (BW) it seems to be at 20MHz by default
  Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/ChannelWidth", UintegerValue (40));
  // Disable Short Guard Interval: Add ghost signal to the carrier in order to reduce the collision probability with the data (multipath fading etc..)
  Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/HtConfiguration/ShortGuardIntervalSupported", BooleanValue (false));
  Config::Connect("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/State/State", MakeCallback(&PhyStateTrace));



  // Setup routing tables
  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll();


  // Run Simulations
  Simulator::Stop (Seconds (SIM_TIME));
  Simulator::Run ();

  for(auto &flow: monitor->GetFlowStats ()){
    NS_LOG_UNCOND("Flow "<< flow.first << " ends at " << flow.second.timeLastRxPacket.GetSeconds() << "s");
  }

  //std::string fname = "NameOfFile"+std::to_string(topo)+"_"+std::to_string(flow)+"_"+std::to_string(nNodePair)+"_"+std::to_string(dataSize)+"_"+std::to_string(seed)+".xml";
  //monitor->SerializeToXmlFile(fname, true, true);

  int i=1;
  for(ApplicationContainer::Iterator n = Sinks.Begin (); n != Sinks.End (); n++){
    ns3::Ptr<Application> app=*n;
    PacketSink& ps=dynamic_cast<PacketSink&>(*app);
    NS_LOG_UNCOND("Sink "<< i << " received " << ps.GetTotalRx() << " bytes");
    i++;
  }

  double totEnergy = 0;

  for (std::vector<DeviceEnergyModelContainer>::iterator it = energyModels.at(0).begin() ; it != energyModels.at(0).end(); ++it) {
    DeviceEnergyModelContainer::Iterator energyIt = (*it).Begin();//  ->Begin();
    while(energyIt!= (*it).End()) {
      NS_LOG_UNCOND("Device consumed " <<  (*energyIt)->GetTotalEnergyConsumption() << " J");
      totEnergy+=(*energyIt)->GetTotalEnergyConsumption();
      energyIt++;
    }
  }
  for (std::vector<DeviceEnergyModelContainer>::iterator it = energyModels.at(1).begin() ; it != energyModels.at(1).end(); ++it) {
    DeviceEnergyModelContainer::Iterator energyIt = (*it).Begin();//  ->Begin();
    while(energyIt!= (*it).End()) {
      NS_LOG_UNCOND("Dyn consumed " <<  (*energyIt)->GetTotalEnergyConsumption() << " J");
      energyIt++;
    }
  }

  std::cout << "Tot energy: "<<totEnergy<< std::endl;

  float totDurIDLE = 0, totDurRx = 0, totDurTx = 0, totDurBusy = 0, totDurSleep = 0, totDurSwitching = 0, totDurOff = 0;

  for (auto e : nodeTimers) {
    totDurIDLE += e.second.second[1];
    totDurRx += e.second.second[2];
    totDurTx += e.second.second[3];
    totDurBusy += e.second.second[4];
    totDurSleep += e.second.second[5];
    totDurSwitching += e.second.second[6];
    totDurOff += e.second.second[7];

    std::cout << "Node timers: "<< e.first << ","<< e.second.second[1]<<","<<
      e.second.second[2]<<","<<
      e.second.second[3]<<","<<
      e.second.second[4]<<","<<
      e.second.second[5]<<","<<
      e.second.second[6]<<","<<
      e.second.second[7]<<std::endl;
  }
  std::cout << "Overall timers: "<<totDurIDLE<<" "<<
      totDurRx<<" "<<
      totDurTx<<" "<<
      totDurBusy<<" "<<
      totDurSleep<<" "<<
      totDurSwitching<<" "<<
      totDurOff<<std::endl;
  // Finish
  Simulator::Destroy ();
  return(0);
}
