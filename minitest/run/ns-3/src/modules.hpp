#ifndef MODULES_HPP
#define MODULES_HPP

#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/string.h"
#include "ns3/log.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/ssid.h"
#include "ns3/mobility-helper.h"
#include "ns3/on-off-helper.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/mobility-model.h"
#include "ns3/packet-sink.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/udp-echo-helper.h"
#include "ns3/tcp-westwood.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/constant-position-mobility-model.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/node-list.h"
#include "ns3/rng-seed-manager.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/energy-module.h"
#include "ns3/wifi-radio-energy-model-helper.h"

using namespace ns3;

//#define SIM_TIME 60 // 5mins simulations
#define SIM_TIME 180
// ---------- Global Variables ---------
extern ApplicationContainer Sinks;

// ---------- Typedef ----------
typedef std::pair<NetDeviceContainer,NetDeviceContainer> WifiNetDev;

// ---------- platform.cc ----------
//void buildT1(short nNodePair, short flowConf, int dataSize, double radius);
//void buildT2(short nNode, short flowConf, int dataSize, double radius);
std::vector<std::vector<DeviceEnergyModelContainer>> buildT1(short nNodePair, short flowConf, int dataSize, double radius);
std::vector<std::vector<DeviceEnergyModelContainer>> buildT2(short nNode, short flowConf, int dataSize, double radius);

#endif
