#!/usr/bin/awk -f

BEGIN {
    consDyn=0
    print("nodeID,IDLE,Rx,Tx,Busy,Sleep,Switching,Off")
}

/Node timers: / {
    print($3)
}
