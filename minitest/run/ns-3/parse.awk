#!/usr/bin/awk -f

BEGIN {
    simTime=0
    simTimeOffset=10
    consTot=0
    consDyn=0
}

/Flow/ && /ends at/ {
    if($5-0 > simTime) # -0 to cast into a number
        simTime=$5-0
}

/Tot energy:/ {
    consTot=$3
}

/Dyn consumed/ {
    consDyn+=$3
}

/Overall timers:/ {
    durIdle=$3
    durRx=$4
    durTx=$5
    durBusy=$6
}


END {
    print("simTime="simTime-simTimeOffset";simTimeOffset="simTimeOffset";consTot="consTot";consDyn="consDyn";durIdle="durIdle";durRxTx="durRx+durTx";durRx="durRx";durTx="durTx";durBusy="durBusy)
}
