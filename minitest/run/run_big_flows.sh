#!/bin/bash
set -e -o pipefail

########## Pay attention: Please install R lib in personal directory before running this script

wai=$(dirname $(readlink -f $0))
[ -e "/tmp/args.sh" ] && onG5K=1 || onG5K=0
sg=${wai}/simgrid/
ns=${wai}/ns-3/
csv=${wai}/results.csv
nSub=30 # Number of parrallel simulations
header="simulator,simTime,simTimeOffset,topo,dataSize,nSTA,bitRate,date,corrFactor,radius,seed,name,useCorrModel,useNS3,duration,consTot,consDyn, timersFile,durIdle,durRxTx,durRx,durTx,durBusy" && [ ! -e "$csv" ] && echo $header > $csv
#header="simulator,simTime,simTimeOffset,topo,dataSize,nSTA,bitRate,date,corrFactor,radius,seed,name,useCorrModel,useNS3,duration" && [ ! -e "$csv" ] && echo $header > $csv
[ $onG5K -eq 1 ] && date=$(cat ${wai}/ts.txt) || date=$(date "+%s")
[ $onG5K -eq 1 ] && logs="/tmp/logs/" || logs="${wai}/logs/$date/"
rm -rf "$logs" && mkdir -p "$logs"

run_once() {
    args="T${topo}_dataSize${dataSize}_nNode${nNode}_nNodePair${nNodePair}_radius${radius}_seed${seed}_radius${radius}_flowConf${flowConf}"
    nsout="${logs}/NS3_${args}.log"
    sgout="${logs}/SG_${args}.log"

    [ $topo -eq 1 ] && nSTA=$(( nNodePair * 2 )) || nSTA=$nNode

    echo -e "\e[1;32m---------- RUN nnode=${nNode} nPair=${nNodePair}---------\e[0m" >&2
    # Run ns-3 simulations
    tStart=$(date "+%s")
#    [ $useNS3 -eq 1 ] && { make -sC $ns ARGS="--topo=$topo --flow=$flowConf --dataSize=$dataSize --nNode=$nNode --nNodePair=$nNodePair --radius=$radius --seed=$seed" run |& tee $nsout >&2; }
    [ $useNS3 -eq 1 ] && { make -sC $ns ARGS="--topo=$topo --flow=$flowConf --dataSize=$dataSize --nNode=$nNode --nNodePair=$nNodePair --radius=$radius --seed=$seed" run |& tee $nsout >&2; }
    tNS3=$(( $(date "+%s") - tStart))

    # Run simgrid simulations
    tStart=$(date "+%s")
    platform=/tmp/${args}_platform.xml
    $sg/gen-platform.py $topo $nNodePair $nNode $bitRate 1 0.0026 > ${platform}
    make -sC $sg ARGS="${platform} $topo $dataSize $flowConf --cfg=plugin:link_energy_wifi --log=link_energy_wifi.thres:debug" run |& tee $sgout >&2
    tSG=$(( $(date "+%s") - tStart))
    echo -e '\e[1;32m---------- RUN END ---------\e[0m\n' >&2

    # Parse logs
    #    [ $useNS3 -eq 1 ] && ns_bash=$(cat $nsout|$ns/parse.awk)
    [ $useNS3 -eq 1 ] && ns_bash=$(cat $nsout|$ns/parse.awk)
    [ $useNS3 -eq 1 ] && $(awk -f $ns/parse2.awk $nsout > ${logs}/ns_timers_${args}.csv)
    sg_bash=$(cat $sgout|$sg/parse.awk)

    ##### Gen CSV #####
    exec 100>>$csv
    flock -x 100 # Lock the file
    if [ $useNS3 -eq 1 ]
    then
#        eval "$ns_bash"
	#        echo "ns3,$simTime,$simTimeOffset,$topo,$dataSize,$nSTA,NA,$date,NA,$radius,$seed,$name,$useCorrModel,$useNS3,$tNS3" >&100
	eval "$ns_bash"
	echo "!!!!!!!ns3: consDyn: $consDyn consTot: $consTot"
#    consDyn=$(python -c "print($consTot-(60-($simTime))*($nSTA+1)*0.82)" 2>&1)
        echo "ns-3,$simTime,$simTimeOffset,$topo,$dataSize,$nSTA,NA,$date,NA,$radius,$seed,$name,$useCorrModel,$useNS3,$tNS3,$consTot,$consDyn,ns_timers_${args}.csv,${durIdle},${durRxTx},${durRx},${durTx},${durBusy}" >&100

    fi
    eval "$sg_bash"
    #    echo "sgmodif,$simTime,$simTimeOffset,$topo,$dataSize,$nSTA,$bitRate,$date,$correction_factor,$radius,$seed,$name,$useCorrModel,$useNS3,$tSG" >&100
    echo "!!!!!!!Sg: consDyn: $consDyns consTot: $consTots"
    echo "simgrid_0,$simTime,$simTimeOffset,$topo,$dataSize,$nSTA,$bitRate,$date,NA,$radius,$seed,$name,$useCorrModel,$useNS3,$tSG,$consTots,$consDyns,NA,${durIdle},${durRxTx},${durRx},${durTx},${durBusy}" >&100

    flock -u 100 # Unlock the file
}


if [ $onG5K -eq 1 ]
then
    csv="/tmp/results.csv"
    rm -rf /tmp/finish.txt
    rm -rf $csv
    over=$(cat /tmp/args.sh|wc -l)
    i=0
    while IFS= read -r line
    do
        eval "$line"
        ( run_once; ) & # Launch as subprocess
        i=$(( i + 1 ))
        echo $i over $over > /tmp/avancement.txt
        while [ $(pgrep -P $$ | wc -l) -ge $nSub ] # Until we have the max of subprocess we wait
        do
            sleep 3
        done
    done < /tmp/args.sh
    wait # Be sure evething is done
else
    dataSizes=$(seq 1000000 5000000 12000000)
#    dataSizes=50000000
#    dataSizes=$(seq 1 50000 250000)
    topos=1
    nNodes=1 #$(seq 2 8)
    nNodePairs=$(seq 1 7)
    flowConf=2
    bitRates=44.1  #54 #$(seq 54 15 110) # bitRate in Mbps
    radius=10
    seeds=$(seq 1 2)
    name="local"
    useNS3=1
    useCorrModel=0

    for dataSize in ${dataSizes}
    do
        for topo in ${topos}
        do
	    for nNode in ${nNodes}
	    do
                for nNodePair in ${nNodePairs}
                do
		    for seed in ${seeds}
		    do
			for bitRate in ${bitRates}
			do
                            run_once &
			done
                    done
		    wait
		done
	    done
        done
    done
fi
mv results.csv ${logs}/results.csv
if [ $onG5K -ne 1 ]
then
    # Do data analysis
    echo -e '\n\e[1;32m---------- Running R data analysis ---------\e[0m'
    R --no-save -q -e 'source("'${wai}/analysis.R'")'
    echo -e '\e[1;32m---------- Done -----------\e[0m'

    # Generate pdf
    echo -e '\n\e[1;32m---------- Generating PDF ---------\e[0m'
    ${wai}/gen-pdf.sh
    pkill -SIGHUP mupdf # Refresh pdf viewer
    echo -e '\e[1;32m---------- Done -----------\e[0m'
else
    touch /tmp/finish.txt
fi
