#!/usr/bin/env bash

wai=$(dirname $(readlink -f "$0")) # Current script directory
logsPath="${wai}/logs/"

for exp in $(ls ${logsPath})
do
    out="${logsPath}/${exp}.log"
    echo "Start exp: $exp"
    for file in $(ls "${logsPath}/${exp}/"|grep -v "logs.org") # just in case avoid logs.org
    do
        echo "##### $file" >> $out
        cat "${logsPath}/${exp}/${file}" >> $out
        rm "${logsPath}/${exp}/${file}"
    done
done

