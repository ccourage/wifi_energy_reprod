# wifi_energy_reprod
This repository contains all information required to launch and reproduce the
results from the experiments performed in the context of my Master internship in
the Myriads team about the modeling of the energy consumption of WiFi
communications at a large scale.

## Description

## Simgrid and ns-3 versions

We use:
- ns-3: 3.33
- simgrid: with commits of the nonlinear model (currently on my fork: https://framagit.org/klement/simgrid/ commit hash: c46b646825329b6615845f41bd2bd6198f2b29a5). Soon in SimGrid's official code repo

### journal.org

We provide a notebook in the file journal.org, containing information about the
experiments and the work performed in the course of this internship.

### Minitest

The minitest directory contains the code used to perform the energy measurements
in the first experiment we provide in the report (i.e. a single access point and
pairs of nodes communicating).

The code in this directory is a modification of the code produced by Loic Guegan
to validate the WiFi performance model of SimGrid. We modified it to add energy
measurements and to parse the interesting information we required.

### Experiments

The experiments directory contains the code we used to perform the other
measurements we made. This is the repository in which you will find the code and
log results for the gwifi and heterogeneous scenarios.

### Results_report

The results_report directory contains the data we used in the course of our
report, which you can reproduce by yourself by running the experiments.

### expetest

This repository does not contain experiments used in the report. It is a set of
examples of simple platform and deployment files which can be used to understand
easily the result outputs given by the model we created for SimGrid.

## Reproduce results of the report

### Installation

You can run the script `install_soft.sh` to fetch and build the versions of ns-3
and SimGrid we used to perform our experiments.

SimGrid and ns-3 will be installed under the `~/dep` directory.

If you chose to not use this script, you will need to change some linking
parameters in the Makefiles to build the experiments, as well as the destination
of symbolic links in the minitest directory.

`bash install_soft.sh`

### Run minitest

To reproduce the results we obtained for the `minitest` experiment, go to the
minitest directory and run the 2 scripts `run.sh` and `run2.sh`. The first one
will reproduce the energy values we observed to validate our results with flow
sizes between 1 byte and 50 MB. The second will reproduce the results of the
control frame correction.

`cd minitest && cd simgrid && make && cd .. && cd ns-3 && make && cd .. && bash run.sh`

### Run heterogeneous experiment

The results of the heterogeneous experiment can be reproduced by running the
`launcherhete.sh` script.

`bash launcherhete.sh`

### Run gwifi

The results obtained via the gwifi experiment can be reproduced by running the
`launcherGwifi.sh` and `launcherGwifi2.sh` scripts.

`bash launcherGwifi.sh`

`bash launcherGwifi2.sh`

WARNING: these experiments require a big amount of memory and have a long
execution time. 

THE launcherGwifi2.sh EXECUTION REQUIRES ALMOST 8 HOURS AND 50 GB TO BE PERFORMED. 

### Customize the experiment parameters

The size of packets, amount of nodes, latencies ... can be modified for all
experiments by modifying the values directly within the launchers.

## Authors

Code within the minitest repository is a modification of the code made by Loic
Guegan to validate the WiFi performance model integrated within SimGrid.

The rest of the code is written by me.

# SimGrid energy plugin

The plugin we produced to model the energy consumption of WiFi communications
can be found on [github](https://github.com/klementc/simgrid).
