#!/usr/bin/env bash

wai=$(dirname $(readlink -f "$0")) # Current script directory
#hostFile="${wai}/hosts.txt"
g5kname="ccourageuxsudan"

function ask {
    for host in $(cat $hostFile)
    do
        echo -e "\e[32m---------- Host: $host ----------\e[0m"
        ssh ${g5kname}@$host "$@"
    done
}


[ $# -eq 0 ] && { echo "Usage: $0 <tmp>"; }

case "$1" in
    "tmp" )
        ask "ls /tmp|grep -e platform -e results.csv -e logs -e finish.txt"
        ;;
    "args")
        ask "cat /tmp/args.sh"
	;;
    "results")
        ask "cat /tmp/results.csv"
        ;;
    'tmux')
        ask "tmux list-session"
        ;;
    'kill')
        ask "tmux kill-server"
        ask "touch /tmp/finish.txt"
        ;;
    'avancement')
        ask "cat /tmp/avancement.txt"
        ;;
    'proc')
        ask "ps -e | grep simulator|wc -l"
        ;;
    'finish')
        ask "ls /tmp/finish.txt"
        ;;
    'apt')
        ask "sudo-g5k apt-get -y install tmux libxml2-dev"

esac

