#!/bin/bash
set -e -o pipefail
user=ccourageuxsudan
wai=$(dirname $(readlink -f $0))
host=$(head -n1 ${wai}/hosts.txt)

run (){
    ssh  ${user}@$host $@
}

echo "Compressing simulators..."
tar -cf "ns-3.tar" "ns-3"
tar -cf "simgrid.tar" "simgrid"
rsync -avh "${wai}/ns-3.tar" ${user}@${host}:~/project/
rsync -avh "${wai}/simgrid.tar" ${user}@${host}:~/project/
rsync -avh "${wai}/run.sh" ${user}@${host}:~/project/
run "cd ~/project/ && tar -xvf ns-3.tar"
run "cd ~/project/ && tar -xvf simgrid.tar"

# # Compile simgrid
run "cd ~/project/simgrid/simgrid && rm -rf build && mkdir build && cd build && cmake ../ && make -j32"
run "cd ~/project/simgrid/ && make clean && make -j32"

# Compile ns3
run "cd ~/project/ns-3/ns-3/ && ./waf configure && make clean && ./waf configure && ./waf"
run "cd ~/project/ns-3/ && make clean && make -j32"




