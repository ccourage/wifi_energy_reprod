#!/bin/bash

[ -z "$nSTAPerAP" ] && nSTAPerAP=4
[ -z "$nAP" ] && nAP=150
latency=10
echo "<?xml version='1.0'?>

<!DOCTYPE platform SYSTEM \"https://simgrid.org/simgrid.dtd\">
<platform version=\"4.1\">
  <zone id=\"world\" routing=\"Full\">

    <!-- Gateway zone -->
    <zone id=\"gateway\" routing=\"Full\">
        <host id=\"GW1\" speed=\"100.0Mf,50.0Mf,20.0Mf\" />
        <host id=\"GW2\" speed=\"100.0Mf,50.0Mf,20.0Mf\" />
        <host id=\"GW3\" speed=\"100.0Mf,50.0Mf,20.0Mf\" />

        <host id=\"OUT\" speed=\"100.0Mf,50.0Mf,20.0Mf\" />

        <link id=\"GW1OUT\" sharing_policy=\"SHARED\" bandwidth=\"10Gbps\" latency=\"${latency}ms\"/>
        <link id=\"GW2OUT\" sharing_policy=\"SHARED\" bandwidth=\"10Gbps\" latency=\"${latency}ms\"/>
        <link id=\"GW3OUT\" sharing_policy=\"SHARED\" bandwidth=\"10Gbps\" latency=\"${latency}ms\"/>

        <!-- Routes -->
        <route src=\"GW1\" dst=\"OUT\">
            <link_ctn id=\"GW1OUT\"/>
        </route>
        <route src=\"GW2\" dst=\"OUT\">
            <link_ctn id=\"GW2OUT\"/>
        </route>
        <route src=\"GW3\" dst=\"OUT\">
            <link_ctn id=\"GW3OUT\"/>
        </route>
    </zone>" > ./experiments/xmlfiles/platform_gwifi_${nAP}.xml

# generate AP zones
for ap in $(seq 0 $(($nAP - 1))); do
  echo "    <zone id=\"wlan${ap}\" routing=\"Cluster\">
        <!-- First declare the Access Point (ie, the wifi media) -->
        <link id=\"AP${ap}\" sharing_policy=\"WIFI\" bandwidth=\"42.87Mbps\" latency=\"0ms\" >
            <prop id=\"wifi_watt_values\" value=\"0.82:1.14:0.94:0.10\" />
		<prop id=\"control_duration\" value=\"0.0026\"/>
        </link>
        <!-- Two stations in the wifi zone -->
        <host id=\"STA$(($ap * 4))\" speed=\"100.0Mf,50.0Mf,20.0Mf\" />
        <host id=\"STA$(($ap * 4 + 1))\" speed=\"100.0Mf,50.0Mf,20.0Mf\" />
	<host id=\"STA$(($ap * 4 + 2))\" speed=\"100.0Mf,50.0Mf,20.0Mf\" />
	<host id=\"STA$(($ap * 4 + 3))\" speed=\"100.0Mf,50.0Mf,20.0Mf\" />
	
        <host_link id=\"STA$(($ap * 4))\" up=\"AP${ap}\" down=\"AP${ap}\"/>
        <host_link id=\"STA$(($ap * 4 + 1))\" up=\"AP${ap}\" down=\"AP${ap}\"/>
        <host_link id=\"STA$(($ap * 4 + 2))\" up=\"AP${ap}\" down=\"AP${ap}\"/>
        <host_link id=\"STA$(($ap * 4 + 3))\" up=\"AP${ap}\" down=\"AP${ap}\"/>

        <router id=\"wrouter${ap}\"/>
    </zone>" >> ./experiments/xmlfiles/platform_gwifi_${nAP}.xml
done

for ap in $(seq 0 $(($nAP - 1))); do
    # link to a gateway node
    echo "    <link id=\"Collector${ap}\" sharing_policy=\"SHARED\" bandwidth=\"10Gbps\" latency=\"${latency}ms\" />" >> ./experiments/xmlfiles/platform_gwifi_${nAP}.xml
done

for ap in $(seq 0 $(($nAP - 1))); do
    echo "    <zoneRoute src=\"wlan${ap}\" dst=\"gateway\" gw_src=\"wrouter${ap}\" gw_dst=\"GW$((1 + $ap % 3))\">
      <link_ctn id=\"Collector${ap}\" />
    </zoneRoute>" >> ./experiments/xmlfiles/platform_gwifi_${nAP}.xml
done

echo "  </zone>
</platform>" >> ./experiments/xmlfiles/platform_gwifi_${nAP}.xml
