 /**
  * Almost the same code as loic, but I made it by myself to understand
  * 
  */
 #include "ns3/command-line.h"
 #include "ns3/config.h"
 #include "ns3/string.h"
 #include "ns3/log.h"
 #include "ns3/yans-wifi-helper.h"
 #include "ns3/ssid.h"
 #include "ns3/mobility-helper.h"
 #include "ns3/on-off-helper.h"
 #include "ns3/yans-wifi-channel.h"
 #include "ns3/mobility-model.h"
 #include "ns3/packet-sink.h"
 #include "ns3/packet-sink-helper.h"
 #include "ns3/udp-echo-helper.h"
 #include "ns3/tcp-westwood.h"
 #include "ns3/internet-stack-helper.h"
 #include "ns3/ipv4-address-helper.h"
 #include "ns3/ipv4-global-routing-helper.h"
 #include "ns3/constant-position-mobility-model.h"
 #include "ns3/energy-module.h"
 #include "ns3/wifi-radio-energy-model-helper.h"
 #include "ns3/point-to-point-helper.h"
 #include "ns3/node-list.h"
 #include "ns3/flow-monitor-module.h"
 #include "ns3/applications-module.h"
 #include "ns3/propagation-delay-model.h"

 // C++ library
 #include <iostream> // Why not ?
 #include <utility>  // To use std::pair
 #include <iomanip>  // To use std::setw




 using namespace ns3;

 void callbackEnergy(double old, double newv){
   //NS_LOG_UNCOND("["<< Simulator::Now()<<"] value changed from "<<old<<" to "<<newv);
 }

 void callbackAssoc(ns3::Mac48Address addr) {
   NS_LOG_UNCOND("["<<Simulator::Now()<<"] Station associated!");
 }

 int main(int argc, char** argv){

   uint32_t nNode=1; // Number of sender/receiver pair
   uint32_t nxchg=1; // Number of sender/receiver pair
   uint32_t nPckt=1; // Number of packets per exchange
   uint32_t msgSize=1000; // Choose the amount of data sended by nodes
   uint32_t nbMsg=1;
   double succMsgDelay=0;
   double idleA=0.273;
   int duration=30;
   int verbose = 1;
   int scenario = 0;

   CommandLine cmd;
   cmd.AddValue ("dataSize", "Packet data size sended by senders", msgSize);
   cmd.AddValue ("nNodePair", "Number of sender/receiver pairs", nxchg);
   cmd.AddValue ("idleA", "Current consumed when idle", idleA);
   cmd.AddValue ("duration", "duration of the simulation (seconds)", duration);
   cmd.AddValue ("nbMsg", "how many messages of size dataSize to send", nbMsg);
   cmd.AddValue ("succMsgDelay", "How long between sending 2 successive messages", succMsgDelay);
   cmd.AddValue ("verbose", "Show sinks logs or not (significant size when true for large experiments", verbose);
   cmd.AddValue ("scenario", "Scenario 0=send to messages to AP, Scenario=1 send to another STA", scenario);
   cmd.Parse (argc, argv);

   // often 1500 even for wifi (due to cross network comm)
   // sometimes 1492 due to an additional 8B field
   // real max size: 2312
   Config::SetDefault ("ns3::TcpSocket::SegmentSize", UintegerValue (1500));

   if(verbose)
     LogComponentEnable("PacketSink", LOG_LEVEL_INFO);


   // build network: 2*nbxchg stations and 1 AP
   // each of them communicating via wifi
   NodeContainer STA;
   NodeContainer AP;

   if(scenario==0)
     STA.Create(nxchg+1);
   else 
     STA.Create(2*nxchg);
   
   AP.Create(1);

   YansWifiChannelHelper channel = YansWifiChannelHelper::Default();
   YansWifiPhyHelper wPhy = YansWifiPhyHelper::Default();
   wPhy.SetChannel(channel.Create());

   WifiHelper wHelper;
   wHelper.SetStandard (WIFI_PHY_STANDARD_80211n_5GHZ);
   wHelper.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                               "DataMode", StringValue ("HtMcs7"),
                               "ControlMode", StringValue ("HtMcs0"));
   WifiMacHelper wMac;
   Ssid ssid= Ssid(std::string("network"));
   wMac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));
  
   //create AP
   NetDeviceContainer apNetDevice = wHelper.Install(wPhy, wMac, AP);
   // create STA
   wMac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
   NetDeviceContainer staNetDevice = wHelper.Install(wPhy, wMac, STA);
   // callback to know when STA are associated to AP
   Config::ConnectWithoutContext("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/$ns3::StaWifiMac/Assoc", MakeCallback(&callbackAssoc));


   // Define sensors position/mobility
   /*
   * Mobility not taken into account in simgrid, should we do that here?
   */

   MobilityHelper mobility;
   mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel"); // Sensors are fixed
   Ptr<UniformRandomVariable> X = CreateObject<UniformRandomVariable> ();
   X->SetAttribute ("Min", DoubleValue (0));
   X->SetAttribute ("Max", DoubleValue (10));
   X->SetAttribute("Stream",IntegerValue(1));
   Ptr<UniformRandomVariable> Y = CreateObject<UniformRandomVariable> ();
   Y->SetAttribute ("Min", DoubleValue (0));
   Y->SetAttribute ("Max", DoubleValue (10));
   Y->SetAttribute("Stream",IntegerValue(1));

   mobility.SetPositionAllocator("ns3::UniformDiscPositionAllocator");
   mobility.Install(NodeContainer(STA));  // Ap on the center
   MobilityHelper mobilityAP;
   mobilityAP.SetMobilityModel ("ns3::ConstantPositionMobilityModel"); // Sensors are fixed
   mobility.SetPositionAllocator("ns3::UniformDiscPositionAllocator",
                                 "rho",DoubleValue(10)); // With default value AP should be on the center maybe
   mobility.Install(NodeContainer(AP));

   // create communications
   InternetStackHelper istack;
   istack.Install(NodeContainer(AP, STA));
   Ipv4InterfaceContainer interface;
   Ipv4AddressHelper addrHelper;
   addrHelper.SetBase("12.0.0.0", "255.0.0.0");
   interface = addrHelper.Assign(NetDeviceContainer(apNetDevice, staNetDevice));

   // create flows on port 80
   PacketSinkHelper STASink("ns3::TcpSocketFactory",InetSocketAddress (Ipv4Address::GetAny (), 80)); 

   for(int i=0 ; i<nxchg ; i++) {
     Ipv4Address src, dst;
     src = interface.GetAddress(scenario==0 ? i+2 : 2*i+1);
     dst = interface.GetAddress(scenario==0 ? 1 : (2*i)+2);
    
     for( int j=0 ; j<nbMsg ; j++){
       BulkSendHelper echoHelper ("ns3::TcpSocketFactory", InetSocketAddress (dst, 80));
       echoHelper.SetAttribute("MaxBytes", UintegerValue(msgSize));
       echoHelper.Install(scenario==0 ? STA.Get(i+1) : STA.Get(2*i)).Start(Seconds(2+j*succMsgDelay));
     }
     if(scenario==1)
       STASink.Install(STA.Get((2*i+1)));
   }
   if(scenario==0)
     STASink.Install(STA.Get(0));

   // energy model
   BasicEnergySourceHelper energySource;
   // put very high value to have "non-battery" nodes..
   energySource.Set("BasicEnergySourceInitialEnergyJ", DoubleValue (10000000));

   EnergySourceContainer batteryAP = energySource.Install(AP);
   EnergySourceContainer batterySTA = energySource.Install(STA);

   WifiRadioEnergyModelHelper radioEnergyHelper;
   radioEnergyHelper.Set("IdleCurrentA", DoubleValue (idleA));

   DeviceEnergyModelContainer consumedEnergyAP = radioEnergyHelper.Install(apNetDevice, batteryAP);
   DeviceEnergyModelContainer consumedEnergySTA =  radioEnergyHelper.Install(staNetDevice, batterySTA);
   
   // debug callback for full energy updates
   for(DeviceEnergyModelContainer::Iterator dem = consumedEnergySTA.Begin() ; dem != consumedEnergySTA.End() ; dem++) {
     (*dem)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&callbackEnergy));
   }
   (*consumedEnergyAP.Begin())->TraceConnectWithoutContext("TotalEnergyConsumption",MakeCallback(&callbackEnergy));

   // Don't forget the following
   Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
  
   // Setup Logs
   FlowMonitorHelper flowmon;
   Ptr<FlowMonitor> monitor = flowmon.InstallAll();
   Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());  

   // Run Simulations  
   Simulator::Stop (Seconds (duration));
   Simulator::Run ();

   // flow log
   //monitor->SerializeToXmlFile("log.xml", true, true);

   // print total energy consumption
   DeviceEnergyModelContainer::Iterator energyIt = consumedEnergySTA.Begin();
   while(energyIt!=consumedEnergySTA.End()) {
     NS_LOG_UNCOND("Device consumed " <<  (*energyIt)->GetTotalEnergyConsumption() << " J");
     energyIt++;
   }
   NS_LOG_UNCOND("Device consumed " <<(*consumedEnergyAP.Begin())->GetTotalEnergyConsumption()<<" J");
   NS_LOG_UNCOND("Experiment duration " << duration);
   Simulator::Destroy ();  
   return(0);
 }
