#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/ssid.h"
#include "ns3/mobility-module.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/wifi-radio-energy-model-helper.h"
#include "ns3/energy-module.h"
#include "ns3/packet-sink.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/bulk-send-helper.h"
#include "ns3/log.h"
#include "ns3/multi-model-spectrum-channel.h"
#include "ns3/spectrum-wifi-helper.h"
#include <boost/algorithm/string.hpp>


using namespace ns3;

NS_LOG_COMPONENT_DEFINE("WifiPlatform");

NetDeviceContainer addLink(Ptr<Node> n1, Ptr<Node> n2)
{
  NodeContainer nc (n1, n2);
  PointToPointHelper ptph;
  ptph.SetDeviceAttribute("DataRate", StringValue("10Gbps"));
  ptph.SetChannelAttribute("Delay", StringValue("10ms"));

  return ptph.Install(nc);
}

int main(int argc, char** argv)
{
  double idleA=0.273;
  int verbose=1;
  std::string datagfile="";
  double duration=1800.;
  int nAP = 50;
  int seed = 1;
  int nbStaPerAP = 4;

  CommandLine cmd;
  cmd.AddValue ("verbose", "Show sinks logs or not (significant size when true for large experiments", verbose);
  cmd.AddValue("datagfile", "file containing flow scenario", datagfile);
  cmd.AddValue("duration", "simulated time", duration);
  cmd.AddValue("nAP", "Amount of AP", nAP);
  cmd.AddValue("seed","seed", seed);
  cmd.AddValue("nSTAPerAP","number of stations per access point", nbStaPerAP);
  cmd.Parse (argc, argv);

  ns3::RngSeedManager::SetSeed(seed);
  if(verbose) {
    LogComponentEnable("BulkSendApplication", LOG_LEVEL_INFO);
    LogComponentEnable("PacketSink", LOG_LEVEL_INFO);
  }

  Config::SetDefault ("ns3::TcpSocket::SegmentSize", UintegerValue (1500));


  NodeContainer gateways;
  gateways.Create(3);

  NodeContainer outside;
  outside.Create(1);

  InternetStackHelper stackHelper;
  stackHelper.Install(gateways);
  stackHelper.Install(outside);

  Ipv4AddressHelper ipv4addr;



  // here comes the hard part: wifi
  std::vector<DeviceEnergyModelContainer> energyModels;
  //std::vector<DeviceEnergyModelContainer> energyModelsDyn;

  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll();


  NodeContainer stas;
  NodeContainer aps;
  //add wifi to extremity nodes (50 APs for now)
  for(int i=0;i<nAP;i++){
    // each wlan is composed of one AP and 4 stations connected to it
    NodeContainer wlanSTAs;
    NodeContainer wlanAP;
    wlanSTAs.Create(4);
    wlanAP.Create(1);

    // use the spectrumwifi model
    SpectrumWifiPhyHelper spectrumPhy;

    Ptr<MultiModelSpectrumChannel> spectrumChannel
      = CreateObject<MultiModelSpectrumChannel> ();
    Ptr<FriisPropagationLossModel> lossModel
      = CreateObject<FriisPropagationLossModel> ();
    lossModel->SetFrequency (5.180e9);
    spectrumChannel->AddPropagationLossModel (lossModel);

    Ptr<ConstantSpeedPropagationDelayModel> delayModel
      = CreateObject<ConstantSpeedPropagationDelayModel> ();
    spectrumChannel->SetPropagationDelayModel (delayModel);

    spectrumPhy.SetChannel (spectrumChannel);
    spectrumPhy.SetErrorRateModel ("ns3::TableBasedErrorRateModel");
    spectrumPhy.Set ("ChannelWidth", UintegerValue (40));
    spectrumPhy.Set ("Frequency", UintegerValue (5180));
    spectrumPhy.Set ("TxPowerStart", DoubleValue (20)); // dBm  (1.26 mW)
    spectrumPhy.Set ("TxPowerEnd", DoubleValue (20));

    WifiHelper wifiHelper;
    wifiHelper.SetStandard (WIFI_STANDARD_80211n_5GHZ);

    // We setup this according to:
    // this examples/wireless/wifi-spectrum-per-example.cc
    // and https://en.wikipedia.org/wiki/IEEE_802.11n-2009
    wifiHelper.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                        "DataMode", StringValue ("HtMcs3"), // Before was HtMcs7
                                        "ControlMode", StringValue ("HtMcs3"));


    WifiMacHelper mac;
    std::ostringstream ss1;
    ss1 <<"lan"<<i;
    Ssid ssid = Ssid (ss1.str().c_str());
    mac.SetType ("ns3::StaWifiMac",
                "Ssid", SsidValue (ssid),
                "ActiveProbing", BooleanValue (false));
    NetDeviceContainer staDevices;
    staDevices = wifiHelper.Install (spectrumPhy, mac, wlanSTAs);

    mac.SetType ("ns3::ApWifiMac",
                "Ssid", SsidValue (ssid));
    NetDeviceContainer apDevices;
    apDevices = wifiHelper.Install (spectrumPhy, mac, wlanAP);

    MobilityHelper mobilitySTA;
    mobilitySTA.SetMobilityModel ("ns3::ConstantPositionMobilityModel"); // Sensors are fixed
    mobilitySTA.SetPositionAllocator("ns3::UniformDiscPositionAllocator");
    mobilitySTA.Install(wlanSTAs);
    MobilityHelper mobilityAP;
    mobilityAP.SetMobilityModel ("ns3::ConstantPositionMobilityModel"); // Sensors are fixed
    mobilityAP.SetPositionAllocator("ns3::UniformDiscPositionAllocator",
                                  "rho",DoubleValue(10));
    mobilityAP.Install(wlanAP);

    // energy model
    BasicEnergySourceHelper energySource;
    // put very high value to have "non-battery" nodes..
    energySource.Set("BasicEnergySourceInitialEnergyJ", DoubleValue (10000000));

    EnergySourceContainer batteryAP = energySource.Install(wlanAP);
    EnergySourceContainer batterySTA = energySource.Install(wlanSTAs);

    WifiRadioEnergyModelHelper radioEnergyHelper;

    DeviceEnergyModelContainer consumedEnergyAP = radioEnergyHelper.Install(apDevices, batteryAP);
    DeviceEnergyModelContainer consumedEnergySTA =  radioEnergyHelper.Install(staDevices, batterySTA);
    energyModels.push_back(consumedEnergyAP);
    energyModels.push_back(consumedEnergySTA);

    //radioEnergyHelper.Set("IdleCurrentA", DoubleValue (0));
    //DeviceEnergyModelContainer consumedEnergyAPDyn = radioEnergyHelper.Install(apDevices, batteryAP);
    //DeviceEnergyModelContainer consumedEnergySTADyn =  radioEnergyHelper.Install(staDevices, batterySTA);
    //energyModelsDyn.push_back(consumedEnergyAPDyn);
    //energyModelsDyn.push_back(consumedEnergySTADyn);

    stackHelper.Install(wlanSTAs);
    stackHelper.Install(wlanAP);

    std::ostringstream ss;
    ss <<"11.1."<<4+i<<".0";
    ipv4addr.SetBase(ss.str().c_str(), "255.255.255.0");
    ipv4addr.Assign(apDevices);
    ipv4addr.Assign(staDevices);

    stas.Add(wlanSTAs);
    aps.Add(wlanAP);
    //stas.Add(wifiAPNode);

  }

  ipv4addr.SetBase("10.1.0.0", "255.255.0.0");

  // link AP to gateway
  for(uint32_t i=0;i<aps.GetN();i++) {
    ipv4addr.Assign(addLink(aps.Get(i), gateways.Get(i%3)));
  }

  NetDeviceContainer backboneD = addLink(gateways.Get(0), outside.Get(0));
  backboneD.Add(addLink(gateways.Get(1), outside.Get(0)));
  backboneD.Add(addLink(gateways.Get(2), outside.Get(0)));
  Ipv4InterfaceContainer backboneInt = ipv4addr.Assign(backboneD);

  // create flows on port 80
  PacketSinkHelper STASink("ns3::TcpSocketFactory",InetSocketAddress (Ipv4Address::GetAny (), 80));

  // install sink on every station
  for(uint32_t i=0;i<stas.GetN();i++) {
    STASink.Install(stas.Get(i));
  }
  for(uint32_t i=0;i<aps.GetN();i++) {
    STASink.Install(aps.Get(i));
  }
  STASink.Install(outside.Get(0));
  STASink.Install(gateways.Get(0));

  std::ifstream stream(datagfile/*"../datagwifi.txt"*/);

  std::string line;
  double delayMax = 0;

  while(std::getline(stream, line)){

    std::vector<std::string> fields;
    boost::algorithm::split(fields, line, boost::is_any_of(","));

    int src = std::stoi(fields[0]);
    std::string dst = fields[1];
    int size = std::stoi(fields[2]);
    int nbCom = std::stoi(fields[3]);
    double delay = std::stod(fields[4]);
    double start = std::stod(fields[5]);


    std::cout << src<< "->"<<dst<<":"<<size<<", "<<nbCom<<" times, delay:"<<delay<<std::endl;

    for(int j=0;j<nbCom;j++) {
      Ipv4Address dstAddr = (outside.Get(0)->GetObject<Ipv4>())->GetAddress(1,0).GetLocal();
      BulkSendHelper echoHelper ("ns3::TcpSocketFactory", InetSocketAddress (dstAddr, 80));
      echoHelper.SetAttribute("MaxBytes", UintegerValue(size));
      echoHelper.SetAttribute("StartTime", TimeValue(Seconds(start+delay*(j))));
      NS_LOG_UNCOND("Experiment: start at "<<start<<" comm  size: " << size << " "<<stas.Get(src)->GetObject<Ipv4>()->GetAddress(1,0).GetLocal()<<"->"<<dstAddr);

      echoHelper.Install(stas.Get(src)).Start(Seconds(start+delay*(j)));
    }
  }

  // 3 gateway nodes, 1 "outside" node
 Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/ChannelWidth", UintegerValue (40));
  // Disable Short Guard Interval: Add ghost signal to the carrier in order to reduce the collision probability with the data (multipath fading etc..)
  Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/HtConfiguration/ShortGuardIntervalSupported", BooleanValue (false));


  //Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
  Simulator::Stop (Seconds (duration));

  Simulator::Run();

  for (std::vector<DeviceEnergyModelContainer>::iterator it = energyModels.begin() ; it != energyModels.end(); ++it) {
    DeviceEnergyModelContainer::Iterator energyIt = (*it).Begin();//  ->Begin();
    while(energyIt!= (*it).End()) {
      NS_LOG_UNCOND("Device consumed " <<  (*energyIt)->GetTotalEnergyConsumption() << " J");
      energyIt++;
    }
  }

  /*for (std::vector<DeviceEnergyModelContainer>::iterator it = energyModelsDyn.begin() ; it != energyModelsDyn.end(); ++it) {
    DeviceEnergyModelContainer::Iterator energyIt = (*it).Begin();//  ->Begin();
    while(energyIt!= (*it).End()) {
      NS_LOG_UNCOND("Dyn consumed " <<  (*energyIt)->GetTotalEnergyConsumption() << " J");
      energyIt++;
    }
  }*/

  std::string fname = "NameOfFile.xml";
  monitor->SerializeToXmlFile(fname, true, true);

  for(auto &flow: monitor->GetFlowStats ()){
    NS_LOG_UNCOND("Flow "<< flow.first << " ends at " << flow.second.timeLastRxPacket.GetSeconds() << "s");
  }

  Simulator::Destroy();
  return 0;
}
