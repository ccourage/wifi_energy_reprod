#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/ssid.h"
#include "ns3/mobility-module.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/wifi-radio-energy-model-helper.h"
#include "ns3/energy-module.h"
#include "ns3/packet-sink.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/bulk-send-helper.h"
#include "ns3/log.h"
#include <boost/algorithm/string.hpp>


using namespace ns3;

NS_LOG_COMPONENT_DEFINE("WifiPlatform");

NetDeviceContainer addLink(Ptr<Node> n1, Ptr<Node> n2)
{
  NodeContainer nc (n1, n2);
  PointToPointHelper ptph;
  ptph.SetDeviceAttribute("DataRate", StringValue("100Mbps"));
  ptph.SetChannelAttribute("Delay", StringValue("10ms"));
  
  return ptph.Install(nc);
}

int main(int argc, char** argv)
{
  double idleA=0.273;
  int verbose=1;
  std::string fpath="../dataDogBone.txt";

  CommandLine cmd;
  cmd.AddValue("idleA", "Current consumed when idle", idleA);
  cmd.AddValue ("verbose", "Show sinks logs or not (significant size when true for large experiments", verbose);
  cmd.AddValue ("data","File describing data flows",fpath);
  cmd.Parse (argc, argv);
  
  if(verbose) {
    LogComponentEnable("BulkSendApplication", LOG_LEVEL_INFO);
    LogComponentEnable("PacketSink", LOG_LEVEL_INFO);
  }

  // bone network, add wifi nodes later
  NodeContainer backbone;
  backbone.Create(2);

  NodeContainer lan1;
  lan1.Add(backbone.Get(0));
  lan1.Create(2);

  NodeContainer lan2;
  lan2.Add(backbone.Get(1));
  lan2.Create(2);

  InternetStackHelper stackHelper;
  stackHelper.Install(lan1);
  stackHelper.Install(lan2);
  
  Ipv4AddressHelper ipv4addr;
  // backbone
  ipv4addr.SetBase("10.1.1.0", "255.255.255.0");
  NetDeviceContainer backboneD = addLink(backbone.Get(0), backbone.Get(1));
  Ipv4InterfaceContainer backboneInt = ipv4addr.Assign(backboneD);

  // first org wired network
  ipv4addr.SetBase("10.1.2.0", "255.255.255.0");
  NetDeviceContainer lan1D = addLink(lan1.Get(1), backbone.Get(0));
  lan1D.Add(addLink(lan1.Get(2), backbone.Get(0)));
  Ipv4InterfaceContainer lan1Int = ipv4addr.Assign(lan1D);

  // second org wired network
  ipv4addr.SetBase("10.1.3.0", "255.255.255.0");
  NetDeviceContainer lan2D = addLink(backbone.Get(1), lan2.Get(1));
  lan2D.Add(addLink(backbone.Get(1), lan2.Get(2)));
  Ipv4InterfaceContainer lan2Int = ipv4addr.Assign(lan2D);

  std::vector<DeviceEnergyModelContainer> energyModels;
  //container for all sta nodes (reused for communications later)
  NodeContainer stas;

  //add wifi to extremity nodes
  for(int i=0;i<4;i++){
    NodeContainer wifiStaNodes;
    NodeContainer wifiAPNode;
    wifiStaNodes.Create(4);
    if(i==0)
      wifiAPNode = lan1.Get(1);
    if(i==1)
      wifiAPNode = lan1.Get(2);
    if(i==2)
      wifiAPNode = lan2.Get(1);
    if(i==3)
      wifiAPNode = lan2.Get(2);

    YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();
    YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();
    phy.SetChannel (channel.Create ());

    WifiHelper wifi;
    wifi.SetRemoteStationManager ("ns3::AarfWifiManager");

    WifiMacHelper mac;
    std::ostringstream ss1;
    ss1 <<"lan"<<i;
    Ssid ssid = Ssid (ss1.str().c_str());
    mac.SetType ("ns3::StaWifiMac",
                "Ssid", SsidValue (ssid),
                "ActiveProbing", BooleanValue (false));

    NetDeviceContainer staDevices;
    staDevices = wifi.Install (phy, mac, wifiStaNodes);

    mac.SetType ("ns3::ApWifiMac",
                "Ssid", SsidValue (ssid));

    NetDeviceContainer apDevices;
    apDevices = wifi.Install (phy, mac, wifiAPNode);

    MobilityHelper mobilitySTA;
    mobilitySTA.SetMobilityModel ("ns3::ConstantPositionMobilityModel"); // Sensors are fixed
    mobilitySTA.SetPositionAllocator("ns3::UniformDiscPositionAllocator");  
    mobilitySTA.Install(wifiStaNodes); 
    // Ap on the center
    MobilityHelper mobilityAP;
    mobilityAP.SetMobilityModel ("ns3::ConstantPositionMobilityModel"); // Sensors are fixed
    mobilityAP.SetPositionAllocator("ns3::UniformDiscPositionAllocator",
                                  "rho",DoubleValue(10));
    mobilityAP.Install(wifiAPNode);

/*    MobilityHelper mobility;

    mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                  "MinX", DoubleValue (0),
                                  "MinY", DoubleValue (0),
                                  "DeltaX", DoubleValue (5.0),
                                  "DeltaY", DoubleValue (5.0),
                                  "GridWidth", UintegerValue (3),
                                  "LayoutType", StringValue ("RowFirst"));

    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    mobility.Install (wifiStaNodes);

    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    mobility.Install (wifiAPNode);
*/
    // energy model
    BasicEnergySourceHelper energySource;
    // put very high value to have "non-battery" nodes..
    energySource.Set("BasicEnergySourceInitialEnergyJ", DoubleValue (10000000));

    EnergySourceContainer batteryAP = energySource.Install(wifiAPNode);
    EnergySourceContainer batterySTA = energySource.Install(wifiStaNodes);

    WifiRadioEnergyModelHelper radioEnergyHelper;
    radioEnergyHelper.Set("IdleCurrentA", DoubleValue (idleA));

    DeviceEnergyModelContainer consumedEnergyAP = radioEnergyHelper.Install(apDevices, batteryAP);
    DeviceEnergyModelContainer consumedEnergySTA =  radioEnergyHelper.Install(staDevices, batterySTA);
    energyModels.push_back(consumedEnergyAP);
    energyModels.push_back(consumedEnergySTA);


    stackHelper.Install(wifiStaNodes);

    std::ostringstream ss;
    ss <<"11.1."<<4+i<<".0";
    ipv4addr.SetBase(ss.str().c_str(), "255.255.255.0");
    ipv4addr.Assign(apDevices);
    ipv4addr.Assign(staDevices);

    stas.Add(wifiStaNodes);
    stas.Add(wifiAPNode);

  }

  // create flows on port 80
  PacketSinkHelper STASink("ns3::TcpSocketFactory",InetSocketAddress (Ipv4Address::GetAny (), 80)); 

  // install sink on every station
  for(int i=0;i<stas.GetN();i++) {
    STASink.Install(stas.Get(i));
  }

  std::ifstream stream(fpath);

  std::string line;
  double delayMax = 0;
  while(std::getline(stream, line)){
    
    std::vector<std::string> fields;
    boost::algorithm::split(fields, line, boost::is_any_of(","));
    
    int src = std::stoi(fields[0]);
    int dst = std::stoi(fields[1]);
    int size = std::stoi(fields[2]);
    int nbCom = std::stoi(fields[3]);
    double delay = std::stod(fields[4]);

    // to adjust the duration of the experiment
    if(delay*nbCom>delayMax) {
      delayMax = delay*nbCom;
    }
    std::cout << src<< "->"<<dst<<":"<<size<<", "<<nbCom<<" times, delay:"<<delay<<std::endl;

    for(int j=0;j<nbCom;j++) {
      Ipv4Address dstAddr = (stas.Get(dst)->GetObject<Ipv4>())->GetAddress(1,0).GetLocal();
      BulkSendHelper echoHelper ("ns3::TcpSocketFactory", InetSocketAddress (dstAddr, 80));
      echoHelper.SetAttribute("MaxBytes", UintegerValue(size));
      echoHelper.SetAttribute("StartTime", TimeValue(Seconds(delay*(j+1))));
      NS_LOG_UNCOND("Experiment: comm  size: " << 10 << " "<<stas.Get(src)->GetObject<Ipv4>()->GetAddress(1,0).GetLocal()<<"->"<<dstAddr);

      echoHelper.Install(stas.Get(src)).Start(Seconds(delay*(j+1)));
    }
  }


  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
  Simulator::Stop (Seconds (delayMax+5));


  Simulator::Run();

  for (std::vector<DeviceEnergyModelContainer>::iterator it = energyModels.begin() ; it != energyModels.end(); ++it) {
    DeviceEnergyModelContainer::Iterator energyIt = (*it).Begin();//  ->Begin();
    while(energyIt!= (*it).End()) {
      NS_LOG_UNCOND("Device consumed " <<  (*energyIt)->GetTotalEnergyConsumption() << " J");
      energyIt++;
    }
  }

  Simulator::Destroy();
  return 0;
}
