 /**
  * Almost the same code as loic, but I made it by myself to understand
  * 
  */
 #include "ns3/command-line.h"
 #include "ns3/config.h"
 #include "ns3/string.h"
 #include "ns3/log.h"
 #include "ns3/yans-wifi-helper.h"
 #include "ns3/ssid.h"
 #include "ns3/mobility-helper.h"
 #include "ns3/on-off-helper.h"
 #include "ns3/yans-wifi-channel.h"
 #include "ns3/mobility-model.h"
 #include "ns3/packet-sink.h"
 #include "ns3/packet-sink-helper.h"
 #include "ns3/udp-echo-helper.h"
 #include "ns3/tcp-westwood.h"
 #include "ns3/internet-stack-helper.h"
 #include "ns3/ipv4-address-helper.h"
 #include "ns3/ipv4-global-routing-helper.h"
 #include "ns3/constant-position-mobility-model.h"
 #include "ns3/energy-module.h"
 #include "ns3/wifi-radio-energy-model-helper.h"
 #include "ns3/point-to-point-helper.h"
 #include "ns3/node-list.h"
 #include "ns3/flow-monitor-module.h"
 #include "ns3/applications-module.h"
 #include "ns3/propagation-delay-model.h"

 // C++ library
 #include <iostream> // Why not ?
 #include <utility>  // To use std::pair
 #include <iomanip>  // To use std::setw

 using namespace ns3;

// sale mais pratique ^^
std::vector<DeviceEnergyModelContainer> energyModels;
std::vector<DeviceEnergyModelContainer> energyModelsDyn;
DeviceEnergyModelContainer consumedEnergySTA;
DeviceEnergyModelContainer consumedEnergyAP;
DeviceEnergyModelContainer consumedEnergySTADyn;
DeviceEnergyModelContainer consumedEnergyAPDyn;

Time durIdleSTA=Time(0);
Time durRxSTA=Time(0);
Time durTxSTA=Time(0);
Time durIdleAP=Time(0);
Time durRxAP=Time(0);
Time durTxAP=Time(0);

Time lastUpdateSta=Time(0);
Time lastUpdateAP=Time(0);
 void callbackEnergyAP(double old, double newv){
   WifiPhyState s = (DynamicCast<WifiRadioEnergyModel>(consumedEnergyAP.Get(0)))->GetCurrentState();
   if(s==WifiPhyState::IDLE){
     durIdleAP+=Simulator::Now()-lastUpdateAP;
   }
   if(s==WifiPhyState::TX){
     durTxAP+=Simulator::Now()-lastUpdateAP;
   }   
   if(s==WifiPhyState::RX){
     durRxAP+=Simulator::Now()-lastUpdateAP;
   }
lastUpdateAP=Simulator::Now();
   NS_LOG_UNCOND("[AP]["<< Simulator::Now()<<"] value changed from "<<old<<" to "<<newv << " "<< s <<" duridle: "<<durIdleAP <<" durTx: "<<durTxAP<<" durRx: "<<durRxAP);
 }
 void callbackEnergySTA(double old, double newv){
   WifiPhyState s = (DynamicCast<WifiRadioEnergyModel>(consumedEnergySTA.Get(0)))->GetCurrentState();
   if(s==WifiPhyState::IDLE){
     durIdleSTA+=Simulator::Now()-lastUpdateSta;
   }
   if(s==WifiPhyState::TX){
     durTxSTA+=Simulator::Now()-lastUpdateSta;
   }   if(s==WifiPhyState::RX){
     durRxSTA+=Simulator::Now()-lastUpdateSta;
   }
   lastUpdateSta= Simulator::Now();
  NS_LOG_UNCOND("[STA]["<< Simulator::Now()<<"] value changed from "<<old<<" to "<<newv << " "<< s <<" duridle: "<<durIdleSTA <<" durTx: "<<durTxSTA<<" durRx: "<<durRxSTA);
 }
 void callbackEnergyAPDyn(double old, double newv){
//  NS_LOG_UNCOND("[AP]["<< Simulator::Now()<<"] value changed from "<<old<<" to "<<newv << " "<< (DynamicCast<WifiRadioEnergyModel>(consumedEnergyAPDyn.Get(0)))->GetCurrentState() );
 }
 void callbackEnergySTADyn(double old, double newv){
//  NS_LOG_UNCOND("[STA]["<< Simulator::Now()<<"] value changed from "<<old<<" to "<<newv << " "<< (DynamicCast<WifiRadioEnergyModel>(consumedEnergySTADyn.Get(0)))->GetCurrentState() );
 }



 void callbackAssoc(ns3::Mac48Address addr) {
   NS_LOG_UNCOND("["<<Simulator::Now()<<"] Station associated!");
 }

 int main(int argc, char** argv){

   uint32_t nNode=1; // Number of sender/receiver pair
   uint32_t nxchg=1; // Number of sender/receiver pair
   uint32_t nPckt=1; // Number of packets per exchange
   uint32_t msgSize=1000; // Choose the amount of data sended by nodes
   uint32_t nbMsg=1;
   double succMsgDelay=0;
   double idleA=0.273;
   int duration=30;
   int verbose = 1;
   int scenario = 0;
   std::string latency = "10ms";

   CommandLine cmd;
   cmd.AddValue ("dataSize", "Packet data size sended by senders", msgSize);
   cmd.AddValue ("nNodePair", "Number of sender/receiver ", nxchg);
   cmd.AddValue ("idleA", "Current consumed when idle", idleA);
   cmd.AddValue ("duration", "duration of the simulation (seconds)", duration);
   cmd.AddValue ("nbMsg", "how many messages of size dataSize to send", nbMsg);
   cmd.AddValue ("succMsgDelay", "How long between sending 2 successive messages", succMsgDelay);
   cmd.AddValue ("verbose", "Show sinks logs or not (significant size when true for large experiments", verbose);
   cmd.AddValue ("scenario", "Scenario 0=send to messages to AP, Scenario=1 send to another STA", scenario);
   cmd.AddValue ("latency", "Latency of PTP link", latency);
   cmd.Parse (argc, argv);

   // often 1500 even for wifi (due to cross network comm)
   // sometimes 1492 due to an additional 8B field
   // real max size: 2312
   Config::SetDefault ("ns3::TcpSocket::SegmentSize", UintegerValue (1500));

   if(verbose)
     LogComponentEnable("PacketSink", LOG_LEVEL_INFO);


   // build network: 2*nbxchg stations and 1 AP
   // each of them communicating via wifi
   NodeContainer STA;
   NodeContainer AP;
   NodeContainer outNode;

   STA.Create(nxchg);
   AP.Create(1);
   outNode.Create(1);
   outNode.Add(AP.Get(0));

   PointToPointHelper pointToPoint;
   pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("10Gbps"));
   pointToPoint.SetChannelAttribute ("Delay", StringValue (latency));

   NetDeviceContainer p2pDevices;
   p2pDevices = pointToPoint.Install (outNode);

   YansWifiChannelHelper channel = YansWifiChannelHelper::Default();
   YansWifiPhyHelper wPhy = YansWifiPhyHelper::Default();
   wPhy.SetChannel(channel.Create());

   WifiHelper wHelper;
   wHelper.SetStandard (WIFI_PHY_STANDARD_80211n_5GHZ);
   wHelper.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                               "DataMode", StringValue ("HtMcs3"),//("HtMcs7"), // essayer 3
                               "ControlMode", StringValue ("HtMcs0")); // laisser a zero
   WifiMacHelper wMac;
   Ssid ssid= Ssid(std::string("network"));
   wMac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));
  
   //create AP
   NetDeviceContainer apNetDevice = wHelper.Install(wPhy, wMac, AP);
   // create STA
   wMac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
   NetDeviceContainer staNetDevice = wHelper.Install(wPhy, wMac, STA);
   // callback to know when STA are associated to AP
   Config::ConnectWithoutContext("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/$ns3::StaWifiMac/Assoc", MakeCallback(&callbackAssoc));


   // Define sensors position/mobility
   /*
   * Mobility not taken into account in simgrid, should we do that here?
   */

   MobilityHelper mobility;
   mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel"); // Sensors are fixed
   Ptr<UniformRandomVariable> X = CreateObject<UniformRandomVariable> ();
   X->SetAttribute ("Min", DoubleValue (0));
   X->SetAttribute ("Max", DoubleValue (10));
   X->SetAttribute("Stream",IntegerValue(1));
   Ptr<UniformRandomVariable> Y = CreateObject<UniformRandomVariable> ();
   Y->SetAttribute ("Min", DoubleValue (0));
   Y->SetAttribute ("Max", DoubleValue (10));
   Y->SetAttribute("Stream",IntegerValue(1));

   mobility.SetPositionAllocator("ns3::UniformDiscPositionAllocator");
   mobility.Install(NodeContainer(STA));  // Ap on the center
   MobilityHelper mobilityAP;
   mobilityAP.SetMobilityModel ("ns3::ConstantPositionMobilityModel"); // Sensors are fixed
   mobility.SetPositionAllocator("ns3::UniformDiscPositionAllocator",
                                 "rho",DoubleValue(10)); // With default value AP should be on the center maybe
   mobility.Install(NodeContainer(AP));

   // create communications
   InternetStackHelper istack;
   istack.Install(NodeContainer(AP, STA));
   NodeContainer l;
   l.Add(outNode.Get(0));
   istack.Install(l);
   Ipv4InterfaceContainer interface;
   Ipv4AddressHelper addrHelper;
   addrHelper.SetBase("12.0.0.0", "255.0.0.0");
   interface = addrHelper.Assign(NetDeviceContainer(apNetDevice, staNetDevice));

   addrHelper.SetBase ("10.1.1.0", "255.255.255.0");
   Ipv4InterfaceContainer p2pInterfaces;
   p2pInterfaces = addrHelper.Assign (p2pDevices);

   // create flows on port 80
   PacketSinkHelper STASink("ns3::TcpSocketFactory",InetSocketAddress (Ipv4Address::GetAny (), 80)); 

   for(int i=0 ; i<nxchg ; i++) {
     Ipv4Address src, dst;
     src = interface.GetAddress(i+1);
     dst = p2pInterfaces.GetAddress(0);
     for( int j=0 ; j<nbMsg ; j++){
       BulkSendHelper echoHelper ("ns3::TcpSocketFactory", InetSocketAddress (dst, 80));
       echoHelper.SetAttribute("MaxBytes", UintegerValue(msgSize));
       echoHelper.Install(STA.Get(i)).Start(Seconds(5+j*succMsgDelay));
     }
   }
   STASink.Install(outNode.Get(0));

   // energy model
   BasicEnergySourceHelper energySource;
   // put very high value to have "non-battery" nodes..
   energySource.Set("BasicEnergySourceInitialEnergyJ", DoubleValue (10000000));
   
   EnergySourceContainer batteryAP = energySource.Install(AP);
   EnergySourceContainer batterySTA = energySource.Install(STA);

    WifiRadioEnergyModelHelper radioEnergyHelper;

    consumedEnergyAP = radioEnergyHelper.Install(apNetDevice, batteryAP);
    consumedEnergySTA =  radioEnergyHelper.Install(staNetDevice, batterySTA);
    energyModels.push_back(consumedEnergyAP);
    energyModels.push_back(consumedEnergySTA);

    radioEnergyHelper.Set("IdleCurrentA", DoubleValue (0));
    consumedEnergyAPDyn = radioEnergyHelper.Install(apNetDevice, batteryAP);
    consumedEnergySTADyn =  radioEnergyHelper.Install(staNetDevice, batterySTA);
    energyModelsDyn.push_back(consumedEnergyAPDyn);
    energyModelsDyn.push_back(consumedEnergySTADyn);


   (*consumedEnergyAP.Begin())->TraceConnectWithoutContext("TotalEnergyConsumption",MakeCallback(&callbackEnergyAP));
   (*consumedEnergySTA.Begin())->TraceConnectWithoutContext("TotalEnergyConsumption",MakeCallback(&callbackEnergySTA));
   (*consumedEnergyAPDyn.Begin())->TraceConnectWithoutContext("TotalEnergyConsumption",MakeCallback(&callbackEnergyAPDyn));
   (*consumedEnergySTADyn.Begin())->TraceConnectWithoutContext("TotalEnergyConsumption",MakeCallback(&callbackEnergySTADyn));


   // Don't forget the following
   Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
  
   // Setup Logs
   FlowMonitorHelper flowmon;
   Ptr<FlowMonitor> monitor = flowmon.InstallAll();
   Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());  

   // Run Simulations  
   Simulator::Stop (Seconds (duration+0.1));
   Simulator::Run ();

   // print total energy consumption
  for (std::vector<DeviceEnergyModelContainer>::iterator it = energyModels.begin() ; it != energyModels.end(); ++it) {
    DeviceEnergyModelContainer::Iterator energyIt = (*it).Begin();//  ->Begin();
    while(energyIt!= (*it).End()) {
      NS_LOG_UNCOND("Device consumed " <<  (*energyIt)->GetTotalEnergyConsumption() << " J");
      energyIt++;
    }
  }

  for (std::vector<DeviceEnergyModelContainer>::iterator it = energyModelsDyn.begin() ; it != energyModelsDyn.end(); ++it) {
    DeviceEnergyModelContainer::Iterator energyIt = (*it).Begin();//  ->Begin();
    while(energyIt!= (*it).End()) {
      NS_LOG_UNCOND("Dyn consumed " <<  (*energyIt)->GetTotalEnergyConsumption() << " J");
      energyIt++;
    }
  }

   NS_LOG_UNCOND("Experiment duration " << duration+0.1 << "(0.1s added)");
   Simulator::Destroy ();  
   return(0);
 }
