#!/usr/bin/awk -f

BEGIN {
    conNsTot=0
    simTimeOffset=5
    durIdleAP=0;
    durTxAP=0;
    durRxAP=0;
    durIdleSTA=0;
    durTxSTA=0;
    durRxSTA=0;
}

/At time/ && /received/ {
    if($3-0 > simTime)
        simTime=$3-0
}
/STA/ {
    gsub("+","")
    gsub("ns","")
    durIdleSTA=$10
    durRxSTA=$12
    durTxSTA=$14
}
/AP/ {
    gsub("+","")
    gsub("ns","")
    durIdleAP=$10
    durRxAP=$12
    dirTxAP=$14
}

/Device consumed/ {

    consNsTot+=$3
}

END {
    print("durIdleNS="durIdleSTA";durRxTxNS="durRxSTA+durTxSTA";consNsTot="consNsTot";simTimeOffset="simTimeOffset";simTimeNS="simTime-simTimeOffset)
}
