#!/usr/bin/awk -f

BEGIN {
    conNsDyn=0
}

/Dyn consumed/ {
    consNsDyn+=$3
}

END {
    print("consNsDyn="consNsDyn)
}
