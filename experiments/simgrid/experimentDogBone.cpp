//
// Created by clement on 20/02/2020.
//

#include "simgrid/s4u.hpp"
#include "xbt/log.h"
#include "simgrid/msg.h"
#include "simgrid/s4u/Activity.hpp"
#include "src/surf/network_cm02.hpp"
#include "src/surf/network_wifi.hpp"

#include <exception>
#include <iostream>
#include <random>
#include <sstream>
#include <string>

#include <fstream>
#include <boost/algorithm/string.hpp>

XBT_LOG_NEW_DEFAULT_CATEGORY(experience, "Wifi exp");

// used to have the same duration of the experiment between ns3 and simgrid
double DELAYMAX = 0;

static void sender(std::vector<std::string> args);
static void receiver(std::vector<std::string> args);

int main(int argc, char** argv) 
{
  simgrid::s4u::Engine engine(&argc, argv);

  engine.register_function("sender", &sender);
  engine.register_function("receiver", &receiver);

  engine.load_platform(argv[1]);

  // set rate for all hosts (we expect all hosts to be connected to the same AP for now)
  simgrid::kernel::resource::NetworkWifiLink* l;
  size_t nb_host = sg_host_count()-2;
  XBT_INFO("%d STA hosts", nb_host);
  for(int j=1;j<=4;j++){
    l= (simgrid::kernel::resource::NetworkWifiLink *)simgrid::s4u::Link::by_name(
          "AP"+std::to_string(j))->get_impl();
// activate wifi performance decay model of loic
//    if(!l->toggle_decay_model())
//      l->toggle_decay_model();

    for(int i=0 ; i<4 ; i++) {
      l->set_host_rate(simgrid::s4u::Host::by_name("STA"+std::to_string(i+4*(j-1))), 0);
      XBT_INFO("STA%d on AP%d", i+4*(j-1), j);
    }
  }

  XBT_INFO("Launching the experiment");

  std::ifstream stream(argv[2]);

  std::string line;
  while(std::getline(stream, line)){
    
    std::vector<std::string> fields;
    boost::algorithm::split(fields, line, boost::is_any_of(","));
    std::vector<std::string> args;

    int src = std::stoi(fields[0]);
    int dst = std::stoi(fields[1]);
    int size = std::stoi(fields[2]);
    double delay = std::stod(fields[4]);

    if(delay>DELAYMAX) {
      DELAYMAX = delay;
    }
    std::cout << "STA"<<src<< "->STA"<<dst<<":"<<size<<std::endl;

    args.push_back(std::string("STA")+fields[1]+"-STA"+fields[0]); //receiver's mailbox (unique name to not interfere)
    args.push_back(fields[3]); //nb msg
    args.push_back(fields[2]); // size of msg
    args.push_back(fields[4]); // delay
    simgrid::s4u::Actor::create("SND:"+fields[0]+"-"+fields[1], simgrid::s4u::Host::by_name(std::string("STA")+fields[0]), &sender, args);
    std::vector < std::string > argsRcv;
    argsRcv.push_back(std::string("STA")+fields[1]+"-STA"+fields[0]); // receiver's mailbox (unique name to not interfere)
    simgrid::s4u::Actor::create("RCV:"+fields[0]+"-"+fields[1], simgrid::s4u::Host::by_name(std::string("STA")+fields[1]), &receiver, argsRcv);
  }

  engine.run();
  
  
  return 0;
}


static void sender(std::vector<std::string> args)
{
  simgrid::s4u::Mailbox* dst = simgrid::s4u::Mailbox::by_name(args.at(0));
  int nb_com = atoi(args.at(1).c_str());
  int size = atoi(args.at(2).c_str());
  double delay = std::stod(args.at(3).c_str());

   XBT_INFO("SENDING %d msg of size %d to %s (delay %f)", nb_com, size, args.at(0).c_str(), delay);
  for (int i=0 ; i<nb_com ; i++) {
    // wait for sending at the right time
    simgrid::s4u::this_actor::sleep_until(delay*(i+1));
    int* payload = new int();
    *payload = nb_com-i-1;
    XBT_DEBUG("payload: %d, time %f", *payload, delay*(i+1));

    dst->put(payload, size);
  }
  XBT_INFO("finished sending");

  simgrid::s4u::this_actor::sleep_until(DELAYMAX+5);
}

static void receiver(std::vector<std::string> args)
{
  XBT_INFO("RECEIVING");

  simgrid::s4u::Mailbox* myBox = simgrid::s4u::Mailbox::by_name(args.at(0)/*simgrid::s4u::this_actor::get_host()->get_name()*/);

  int* notFinished = (static_cast<int*>(myBox->get()));
  int nbReceived=1;
  XBT_INFO("Received %d messages at time %f", nbReceived, simgrid::s4u::Engine::get_clock());

  //XBT_DEBUG("notfinished: %d", *notFinished);
  while( (*notFinished) != 0) {
    delete notFinished;
    notFinished = (static_cast<int*>(myBox->get()));
    nbReceived++;
    //XBT_DEBUG("notfinished: %d", *notFinished);
    XBT_INFO("Received %d messages at time %f", nbReceived,  simgrid::s4u::Engine::get_clock());
  }

  XBT_INFO("received all messages");

  simgrid::s4u::this_actor::sleep_until(DELAYMAX+5);
}
