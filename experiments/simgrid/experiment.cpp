 //
 // Created by clement on 20/02/2020.
 //

 #include "simgrid/s4u.hpp"
 #include "xbt/log.h"
 #include "simgrid/msg.h"
 #include "simgrid/s4u/Activity.hpp"
 #include "src/surf/network_cm02.hpp"
 #include "src/surf/network_wifi.hpp"

 #include <exception>
 #include <iostream>
 #include <random>
 #include <sstream>
 #include <string>

 XBT_LOG_NEW_DEFAULT_CATEGORY(experience, "Wifi exp");
 double DURATION = 30;
 static void sender(std::vector<std::string> args);
 static void receiver(std::vector<std::string> args);

 int main(int argc, char** argv) 
 {

   // engine
   int scenario = std::stoi(argv[3]);
   simgrid::s4u::Engine engine(&argc, argv);

   engine.register_function("sender", &sender);
   engine.register_function("receiver", &receiver);

   engine.load_platform(argv[1]);
   engine.load_deployment(argv[2]);

   simgrid::kernel::resource::NetworkWifiLink* l= (simgrid::kernel::resource::NetworkWifiLink *)simgrid::s4u::Link::by_name(
           "AP1")->get_impl();
// activate wifi performance decay model of loic
//   if(!l->toggle_decay_model())
//     l->toggle_decay_model();
   // set rate for all hosts (we expect all hosts to be connected to the same AP for now)
   size_t nb_host = sg_host_count();
   XBT_INFO("%d hosts", nb_host);
   for(int i=0 ; i<nb_host-1 ; i++) {
     l->set_host_rate(simgrid::s4u::Host::by_name("STA"+std::to_string(i)), 0);
   }
   if(scenario==0){
     XBT_INFO("scenar 0");
     l->set_host_rate(simgrid::s4u::Host::by_name(std::string("AP")),0);
   }else{
     XBT_INFO("scenar not 0 %s",argv[3]);
   }
   XBT_INFO("Launching the experiment");

   engine.run();
  
   XBT_INFO("Experiment duration %f", DURATION);
   return 0;
 }


 void sender(std::vector<std::string> args)
 {
   std::string mbName = args.at(5);
   simgrid::s4u::Mailbox* dst = simgrid::s4u::Mailbox::by_name(mbName);
   int nb_com = atoi(args.at(2).c_str());
   int size = atoi(args.at(3).c_str());
   double delay = std::stod(args.at(4).c_str());

   XBT_INFO("SENDING %d msg of size %d to %s (delay %f)", nb_com, size, mbName.c_str(), delay);
   for (int i=0 ; i<nb_com ; i++) {
     // wait for sending at the right time
     simgrid::s4u::this_actor::sleep_until(delay*i);
     int* payload = new int();
     *payload = nb_com-i-1;
     XBT_DEBUG("payload: %d, time %f", *payload, delay*i);

     dst->put(payload, size);
   }
   XBT_INFO("finished sending");

   simgrid::s4u::this_actor::sleep_until(DURATION);
 }

 void receiver(std::vector<std::string> args)
 {
   std::string mbName = args.at(1);
   XBT_INFO("RECEIVING on mb %s", mbName.c_str());
   simgrid::s4u::Mailbox* myBox = simgrid::s4u::Mailbox::by_name(mbName);

   int* notFinished = (static_cast<int*>(myBox->get()));
   int nbReceived=1;
   XBT_INFO("Received %d messages at time %f", nbReceived, simgrid::s4u::Engine::get_clock());

   //XBT_DEBUG("notfinished: %d", *notFinished);
   while( (*notFinished) != 0){
     delete notFinished;
     notFinished = (static_cast<int*>(myBox->get()));
     nbReceived++;
     //XBT_DEBUG("notfinished: %d", *notFinished);
     XBT_INFO("Received %d messages at time %f", nbReceived,  simgrid::s4u::Engine::get_clock());
   }

   XBT_INFO("received all messages");

   simgrid::s4u::this_actor::sleep_until(DURATION);
 }
