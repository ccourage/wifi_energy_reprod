#!/usr/bin/awk -f

BEGIN {
    conSgTot=0
    conSgDyn=0
    durTxRx=0
    durIdle=0
}

/finished sending/ {
    gsub("]","")
    simTime=$2+0
#    simTime=$5+0
}

/consumed/ {
    consSgTot+=$7
    consSgDyn+=$10
    durTxRx+=$16 *10e8
    durIdle+=$14 *10e8
}

END {
    print("durTxRxSG="durTxRx";durIdleSG="durIdle";consSgTot="consSgTot";consSgDyn="consSgDyn";simTimeSG="simTime)
}
