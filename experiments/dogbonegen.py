import random
import sys

if( (len(sys.argv)<5)):
    print("Usage: python "+sys.argv[0]+" <nbSTA> <sizeMsg> <nbMsgPerFlow> <delayMax>")
    exit(0)

nbSTA=int(sys.argv[1]) # total number of stations
size=int(sys.argv[2]) # size of each msg
nbCom=int(sys.argv[3]) # nb msg per flow
delayMax=float(sys.argv[4]) # delay will be between 0 and delayMax

def genComm(src, nbSTA, size, nbCom, delay):
    return str(str(src)+","+str(random.randint(0, nbSTA-1))+","+str(size)+","+str(nbCom)+","+str(delay))

for j in range(16):
    print(genComm(j,nbSTA, size, nbCom, random.uniform(0,delayMax)))
