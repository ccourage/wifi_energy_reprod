#!/bin/sh

# Launch your experiment with custom parameters
nAPs="1 3 5 10 15 30 50 75"
#nbSTA=200
durTot="300" # 15 20 25 30"
verbose=0
#verbose=1
seed="1"

# keep these if you do install dependencies with install_soft.sh (see utilities)
export SG_PATH=/home/clem/Code/framagit.org/klement/simgrid/build/lib
export NS3_PATH=~/dep/ns-allinone-3.33/ns-3.33/build/lib #/home/clem/Code/gitlab.inria.fr/lguegan/wifi-flow-level/run/ns-3/ns-3/build/lib
export LD_LIBRARY_PATH=$SG_PATH:$NS3_PATH

# parameters
[ -z "$nAPs" ] && nAPs=150
#[ -z "$nbSTA" ] && nbSTA=200
[ -z "$durTot" ] && durTot=1800
[ -z "$verbose" ] && verbose=1
[ -z "$seed" ] && seed=1

echo "simulator,seed,nAP,nSTA,duration,mem,dur,total,dyn"> ./experiments/logs/gwifi_d${durTot}_1.csv

for dur in $durTot
do
  for nAP in $nAPs
  do
    nSTA=$((4*$nAP))
    nAP=${nAP} bash platf_gwifi.sh
	  python generategwificom.py ${nSTA} $(($dur)) ${seed} > ./experiments/datagwifi_${nSTA}sta_${dur}sec_${seeds}seed.txt

        echo "=== expe dur=$dur seed=$seed nSTA=${nSTA} nAP=$nAP"

	    #/usr/bin/time  -f 'swappedCount: %W cpuTimeU: %U cpuTimeS: %S wallClock: %e peakMemUsage: %M' ./experiments/simgrid/gwifi ./experiments/xmlfiles/platform_gwifi_${nAP}.xml ./experiments/datagwifi_${nSTA}sta_${dur}sec.txt ${dur} ${nAP} --cfg=plugin:link_energy_wifi --log=link_energy_wifi.thres:info --log=root.app:stdout --cfg=network/crosstraffic:0 > ./experiments/logs/log_gwifi_sta${nSTA}_dur${dur}_simgrid 2>&1
	    /usr/bin/time  -f 'swappedCount: %W cpuTimeU: %U cpuTimeS: %S wallClock: %e peakMemUsage: %M' ./experiments/ns3/gwifi --nAP=${nAP} --duration=${dur} --seed=$seed --verbose=${verbose} --datagfile=./experiments/datagwifi_${nSTA}sta_${dur}sec.txt  > ./experiments/logs/log_gwifi_sta${nSTA}_dur${dur}_ns3 2>&1

	    #eval $(cat ./experiments/logs/log_gwifi_sta${nSTA}_dur${dur}_simgrid | awk 'BEGIN{sum=0;dyn=0;memsg=0;dursg=0} /peakMemUsage/{memsg=$10;dursg=$8} /consumed/{sum+=$7;dyn+=$10 } END{print "vsg="sum";vsgDyn="dyn";memsg="memsg";dursg="dursg}')
	    eval $(cat ./experiments/logs/log_gwifi_sta${nSTA}_dur${dur}_ns3 | awk 'BEGIN{sum=0;dyn=0;memns=0;durns=0} /Device consumed/{sum+=$3 } /peakMemUsage/{memns=$10;durns=$8} /Dyn consumed/{dyn+=$3 } END{print "vns="sum";vnsDyn="dyn";memns="memns";durns="durns}')

	    #echo "sg,NAN,$nSTA,$dur,$memsg,$dursg,$vsg,$vsgDyn">> ./experiments/logs/gwifi_d${durTot}_1.csv
	    echo "ns,${seed},${nAP},${nSTA},${dur},${memns},${durns},${vns},${vnsDyn}">> ./experiments/logs/gwifi_d${durTot}_1.csv

  done
done
