mkdir -p ~/dep
cd ~/dep
basedir=$(pwd)
echo "Installing in $basedir"

#echo "Starting install for ns3 (https://gitlab.com/clementcs/ns-3-dev.git)"
#git clone https://gitlab.com/clementcs/ns-3-dev.git
wget https://www.nsnam.org/releases/ns-allinone-3.33.tar.bz2
tar -jxvf ns-allinone-3.33.tar.bz2
cd ns-allinone-3.33/ns-3.33/
./waf configure && ./waf -j8
echo "ns3 installed"


cd $basedir
echo "Starting install for simgrid (https://github.com/klementc/simgrid.git)"
git clone https://github.com/klementc/simgrid.git
cd simgrid
mkdir build && cd build
cmake ..
make -j4
echo "Simgrid installed"

cd $basedir
