# Launch your experiment with custom parameters
maxFlows=1
dataSizes="20000 100000 500000 1000000 2000000 10000000"
delayBetweenFlows=1
nbMsgPerFlow="1 10 20 30"
duration=120
latencies="0 5 10"

# import all required functions
# keep these if you do install dependencies with install_soft.sh (see utilities)
export SG_PATH=~/dep/simgrid/build/lib #/home/clem/code/github.com/klementc/simgrid/build/lib
export NS3_PATH=~/dep/ns-3-dev/build/lib #/home/clem/code/gitlab.io/clementcs/ns-3-dev/build/lib
export LD_LIBRARY_PATH=$SG_PATH:$NS3_PATH
gen_xml(){
    datasize=$1
    nbflows=$2
    idleW=$3
    nbMsgPerFlow=$4
    delayBetweenFlows=$5
    lat=$6

    mkdir -p experiments/xmlfiles
    # gen platform
    filename=experiments/xmlfiles/hete_f${nbflows}_l${lat}.xml

    echo "saving in $filename"

    #header
    echo "<?xml version='1.0'?>
    <!DOCTYPE platform SYSTEM \"http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd\">
    <platform version=\"4.1\">
    <zone id=\"world\" routing=\"Full\">
      
    <zone id=\"WIFI zone\" routing=\"Cluster\">
    " > $filename
    echo "        <link id=\"AP1\" sharing_policy=\"WIFI\" bandwidth=\"54Mbps\" latency=\"0ms\">">>$filename
    echo "            <prop id=\"wifi_watt_values\" value=\"${idleW}:1.14:0.94:0.10\" />">>$filename
    echo "        </link>">>$filename

    for i in $(seq 0 $(($nbflows - 1)))
    do
        echo "        <host id=\"STA${i}\" speed=\"100.0Mf,50.0Mf,20.0Mf\" />
                 <host_link id=\"STA${i}\" up=\"AP1\" down=\"AP1\"/>" >> $filename
    done
    
    echo "    <router id=\"router\"/>
        </zone>
    <zone id=\"gateway\" routing=\"Full\">
        <host id=\"OUT\" speed=\"100.0Mf,50.0Mf,20.0Mf\" />
    </zone>
    <link id=\"outlink\" sharing_policy=\"SHARED\" bandwidth=\"10Gbps\" latency=\"${lat}ms\" />
    <zoneRoute src=\"WIFI zone\" dst=\"gateway\" gw_src=\"router\" gw_dst=\"OUT\">
      <link_ctn id=\"outlink\" />
    </zoneRoute>

    </zone>
</platform>	
    " >>$filename



    # gen deployment
    filename=experiments/xmlfiles/deplhete_s${datasize}_f${nbflows}_n${nbMsgPerFlow}_l${lat}.xml

    echo "<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM \"https://simgrid.org/simgrid.dtd\">
<platform version=\"4.1\">" > $filename

    for i in $(seq 0 $(($nbflows - 1)))
    do
        echo "
  <actor host=\"STA${i}\" function=\"sender\">
    <argument value=\"OUT\"/>
    <argument value=\"${nbMsgPerFlow}\"/>
    <argument value=\"${datasize}\"/>
    <argument value=\"${delayBetweenFlows}\"/>
    <argument value=\"STA${i}_OUT\"/>
  </actor>

  <actor host=\"OUT\" function=\"receiver\">
    <argument value=\"STA${i}_OUT\"/>
  </actor>
  " >> $filename
    done

    echo "</platform>" >>$filename
}


# set parameters if not provided
[ -z "$idleValueSG" ] && idleValueSG=0.82
[ -z "$idleValueNS3" ] && idleValueNS3=0.273 
[ -z "$maxFlows" ] && maxFlows=5
[ -z "$dataSizes" ] && dataSizes="1024 10000 100000 200000"
[ -z "$delayBetweenFlows" ] && delayBetweenFlows=0
[ -z "$nbMsgPerFlow" ] && nbMsgPerFlow=1
[ -z "$verbose" ] && verbose=1
[ -z "$scenar" ] && scenar=0
[ -z "$duration" ] && duration=60
[ -z "$latencies" ] && latencies=10
# erase previous results
mkdir -p experiments/logs
#rm -rf experiments/logs/*

run() {
    # simgrid
    /usr/bin/time  -f 'swappedCount:%W cpuTimeU:%U cpuTimeS:%S wallClock:%e peakMemUsage:%M' ./experiments/simgrid/expeterogeneous ./experiments/xmlfiles/hete_f${expef}_l${lat}.xml ./experiments/xmlfiles/deplhete_s${size}_f${expef}_n${nbMsg}_l${lat}.xml $duration --cfg=plugin:link_energy_wifi --log=link_energy_wifi.thres:info --cfg=network/crosstraffic:0 > ./experiments/logs/log_hete_s${size}_f${expef}_n${nbMsg}_l${lat}_simgrid 2>&1
    sg_bash=$(cat ./experiments/logs/log_hete_s${size}_f${expef}_n${nbMsg}_l${lat}_simgrid | experiments/simgrid/parse.awk)
    eval "$sg_bash"

    # ns3
    /usr/bin/time  -f 'swappedCount:%W cpuTimeU:%U cpuTimeS:%S wallClock:%e peakMemUsage:%M' ./experiments/ns3/expeterogeneous --duration=$duration --latency=${lat}ms --scenario=${scenar} --verbose=${verbose} --succMsgDelay=${delayBetweenFlows} --nbMsg=${nbMsg} --dataSize=${size} --nNodePair=$expef --idleA=${idleValueNS3} > ./experiments/logs/log_hete_s${size}_f${expef}_n${nbMsg}_l${lat}_ns3 2>&1 
    ns_bash=$(cat ./experiments/logs/log_hete_s${size}_f${expef}_n${nbMsg}_l${lat}_ns3 | experiments/ns3/parse.awk)
    ns_bash2=$(cat ./experiments/logs/log_hete_s${size}_f${expef}_n${nbMsg}_l${lat}_ns3 | experiments/ns3/parse2.awk)
    eval "$ns_bash"
    eval "$ns_bash2"

    lock="./experiments/logs/results_hetero.csv"
    exec 200>>$lock
    flock -x 200
    echo "simgrid,$simTimeSG,NA,$durTxRxSG,$durIdleSG,$expef,$size,$nbMsg,$scenar,$lat,$consSgTot,$consSgDyn" >> ./experiments/logs/results_hetero.csv
    echo "ns3,$simTimeNS,$simTimeOffset,$durRxTxNS,$durIdleNS,$expef,$size,$nbMsg,$scenar,$lat,$consNsTot,$consNsDyn" >> ./experiments/logs/results_hetero.csv
    flock -u 200
}


echo "simulator,simTime,simTimeOffset,durRxTx,durIdle,nbnodes,size,nbMsgPerFlow,scenar,latency,consTot,consDyn"> ./experiments/logs/results_hetero.csv

for nbMsg in ${nbMsgPerFlow}
do
  for size in ${dataSizes}
  do
	for expef in $(seq 1 ${maxFlows})
    do
      for lat in ${latencies}
      do
        echo $nbMsg $size $expef $lat
        gen_xml ${size} ${expef} ${idleValueSG} ${nbMsg} ${delayBetweenFlows} ${lat}
	    run &
      done
    done
    wait
  done
done
